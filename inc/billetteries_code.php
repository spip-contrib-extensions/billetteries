<?php
/**
 * Fonctions du plugin Billetteries relatives au code des billets
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GPL 3
 * @package    SPIP\Billetteries\Fonctions
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Génère un code unique utilisé pour remplir le champ `code` lors de la création d'un billet
 * 
 * Pour l'instant le code retourné est basé sur la date du billet et son numéro SQL
 *
 * @param int
 * 		$id_billet
 * @return string
 * 		Code du billet
**/
function inc_billetteries_code_dist($id_billet) {
	if ($date = sql_getfetsel('date', 'spip_billets', 'id_billet='.intval($id_billet))) {
		$t = strtotime($date);
	}
	else {
		$t = $_SERVER['REQUEST_TIME'];
	}

	// format YYYYMMDDNNNNNN
	$code = date('Ymd', $t) . str_pad(intval($id_billet), 6, '0', STR_PAD_LEFT);

	return $code;
}
