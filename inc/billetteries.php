<?php
/**
 * Fonctions du plugin Billetteries
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt, Concurrences
 * @licence    GPL 3
 * @package    SPIP\Billetteries\Fonctions
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Compléter automatiquement certains champs d'un billet
 *
 * Sous-entendu : au moment de son édition et tant qu'il n'est pas encore "finalisé",
 * c'est à dire à la création ou tant qu'il est encore au panier.
 *
 * La fonction ne fait que retourner les champs éventuellement modifiés.
 * Elle ne met pas à jour le billet, c'est à l'appelant de procéder.
 *
 * Important : il faut que le billet ait déjà un id_billets_type,
 * sinon le prix retourné sera toujours 0.
 *
 * Champs concernés :
 * - id_billetterie : s'il est vide
 * - code : s'il est vide
 * - email : s'il est vide et qu'on a un id_auteur
 * - source : s'il est vide et qu'on est dans le privé
 * - infos : ajout de prix_ht, taxes, et titre_billets_type
 *
 * @param int $id_billet
 *   Numéro du billet
 * @param array $champs
 *   Couples champ => valeur.
 *   Si des champs complétables ne sont pas présents dans la liste, ils seront ajoutés.
 *   Ce ne sont pas nécessairement les valeurs en base :
 *   par ex. des nouvelles valeurs si on est en train de modifier le billet.
 * @param array|null $billet
 *   Ligne SQL du billet si on l'a déjà sous la main
 * @return array
 *   Couples champs => valeurs, certaines étant éventuellement modifiées
 */
function billetteries_billet_completer_champs($id_billet, $champs = array(), $billet = array()) {

	include_spip('base/abstract_sql');

	// Récupérer les valeurs en base si nécessaire
	if (!$billet) {
		$billet = sql_fetsel('*', 'spip_billets', 'id_billet = ' . $id_billet);
	}

	// Garder une trace des modifs
	$champs_avant = $champs;

	// Les champs à compléter
	$champs_a_completer = array(
		'code',
		'email',
		'id_billetterie',
		'source',
		'infos',
	);
	$id_billets_type = intval(isset($champs['id_billets_type']) ? $champs['id_billets_type'] : $billet['id_billets_type']);
	$id_auteur = intval(isset($champs['id_auteur']) ? $champs['id_auteur'] : $billet['id_auteur']);
	foreach ($champs_a_completer as $champ) {
		// Si la valeur n'est pas donnée, récupérer celle en base
		$valeur = (isset($champs[$champ]) ? $champs[$champ] : $billet[$champ]);
		switch ($champ) {

			// Si le code est vide : le générer
			case 'code':
				if (empty($valeur)) {
					$fonction_code = charger_fonction('billetteries_code', 'inc/');
					$code = $fonction_code($id_billet);
					$champs['code'] = $code;
				}
				break;

			// Si l'email est vide : prendre celui de l'auteur associé
			case 'email':
				if (
					empty($valeur)
					and $id_auteur > 0
				) {
					$email = sql_getfetsel('email', 'spip_auteurs', "id_auteur = $id_auteur");
					$champs['email'] = $email;
				}
				break;

			// Si le numéro de billetterie est vide : prendre celui du type de billet
			case 'id_billetterie':
				if (
					empty($valeur)
					and $id_billets_type > 0
				) {
					$id_billetterie = intval(sql_getfetsel('id_billetterie', 'spip_billets_types', "id_billets_type = $id_billets_type"));
					$champs['id_billetterie'] = $id_billetterie;
				}
				break;

			// Si la source est vide et qu'on est dans le privé : indiquer l'admin
			case 'source':
				if (
					empty($valeur)
					and test_espace_prive()
				) {
					$id_admin = (isset($GLOBALS['visiteur_session']['id_auteur']) ? intval($GLOBALS['visiteur_session']['id_auteur']) : '?');
					$source = "admin#{$id_admin}";
					$champs['source'] = $source;
				}
				break;

			// Garder le prix actuel du billet au cas où il change par la suite
			case 'infos':
				// Récupérer les infos actuelles désérialisées
				$infos = array();
				if (!empty($valeur)) {
					try {
						$infos = unserialize($valeur);
					} catch (Exception $e) {
						$erreur = $e->getMessage();
						spip_log("[auto_completer_billet] échec unserialize infos du billet $id_billet : $erreur", 'billetteries' . _LOG_ERREUR);
					}
				}
				$infos = (is_array($infos) ? $infos : array());
				// Ajouter le titre du type de billet
				$titre_billets_type = sql_getfetsel('titre', 'spip_billets_types', "id_billets_type = $id_billets_type");
				$infos['billets_type'] = $titre_billets_type;
				// Ajouter le prix et les taxes
				// Pour les taxes on peut avoir plusieurs taux, on ne garde donc que le montant total pour simplifier
				if (
					$fonction_prix_ht = charger_fonction('ht', 'inc/prix', true)
					and $fonction_taxes = charger_fonction('taxes', 'inc/', true)
				) {
					$infos['prix_ht'] = $fonction_prix_ht('billet', $id_billet);
					$taxes = $fonction_taxes('billet', $id_billet);
					$taxes_montant = array_sum(array_column($taxes, 'montant'));
					$infos['taxes'] = $taxes_montant;
				}
				// Resérialiser
				$infos = serialize($infos);
				$champs['infos'] = $infos;
				break;
		}
	}

	// Un coup pour les plugins (billetteries_profils…)
	$champs = pipeline('billet_completer_champs', array(
		'args' => array(
			'id_billet' => $id_billet,
			'billet' => $billet,
		),
		'data' => $champs
	));

	// Loguer s'il y a des choses modifiées
	// if ($set = array_diff($champs, $champs_avant)) {
	// 	$log_set = json_encode($set);
	// 	spip_log("[billet_completer_champs] compléments automatiques du billet $id_billet : $log_set", 'billetteries' . _LOG_DEBUG);
	// }

	return $champs;
}
