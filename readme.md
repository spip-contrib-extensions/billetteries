# Billetteries
Ce plugin permet la gestion complète d’une ou plusieurs billetteries dans lesquelles on propose des types de billets à acheter. Il est alors possible de commander des billets, grâce aux plugins Commandes et Bank.

## Les concepts

### Les billetteries
Il s'agit de "contenants", dans lesquels on proposera un ou plusieurs types de billets à acheter. Vous pouvez n'en avoir qu'une seule dans tout votre site, ou des dizaines. Les billetteries peuvent être liées à vos autres contenus. Il est donc par exemple possible d'ajouter une billetterie pour chacun de vos événements du plugin Agenda, afin de prendre des billets pour y accéder (que ce soit payant ou gratuit). Chacune peut avoir un quota, si le nombre de places est limitée, il vaudra alors quelque soit les types des billets réservés. Il est possible d'indiquer des dates d'ouverture et de fermeture hors desquelles rien ne pourra être réservé.

### Les types de billets
C'est ce qu'on pourra réserver dans chaque billetterie, avec possiblement un prix même si ce n'est pas obligatoire. Chaque type a de nombreuses options possibles, de quota aussi, d'ouverture et fermeture, etc. Il est par exemple possible de créer des billets de type "early bird" à tarif plus bas que les autres, et qui seront disponibles avant le reste, mais avec un nombre max de ce type.

### Les billets
C'est ce que les gens vont réellement réserver (en achetant ou pas suivant le prix indiqué). Généralement ils sont générés à la suite d'une commande faire avec le formulaire fourni `#FORMULAIRE_COMMANDER_BILLETTERIE`.

## Commander des billets
Un formulaire prêt à l'emploi est fourni pour commander facilement des billets d'une billetterie précise.

Il s'utilise très simplement en fournissant pour quelle billetterie et la page suivante où se rendre. Ensuite tout est calculé automatiquement pour ouvrir ou pas, quels types de billet sont disponibles à réserver, suivant les quotas, dates, etc.

``` html
#FORMULAIRE_COMMANDER_BILLETTERIE{#ID_BILLETTERIE, #URL_PAGE{profil}}
```

Le résultat de ce formulaire sera de créer un panier temporaire dans un tableau en session PHP pour la personne qui visite. Pour effectuer une commande complète, tout est prévu si on utilise le plugin Commandes (et donc le plugin Bank s'il faut payer à la fin). Lorsque ce dernier est présent, alors une vraie commande est générée si la personne valide l'un des formulaires suivants : `#FORMULAIRE_EDITER_AUTEUR`, `#FORMULAIRE_INSCRIPTION` ou `#FORMULAIRE_PROFIL`. Un pipeline `billetteries_generer_commande_apres_formulaires` permet d'en rajouter d'autres si vous avez vos propres méthodes pour demander les informations de la personne acheteuse, mais dans tous les cas il faut un `id_auteur` dans le retour du `traiter`.

À la suite de tout cela, vous avez donc une vraie commande, liée à un compte utilisateur, qui contient en détails de commande tous les types de billets demandés (3 billets early bird, 1 billet tarif réduit, etc). Commande qu'on utilise donc comme habituellement (se référer à la doc du plugin).

## Informations différentes suivant les billets réservés
Si vous utilisez le plugin Profils pour définir quelles informations demander aux gens durant l'inscription ou la vérification des infos, il est possible d'aller encore plus loin en précisant des profils possiblement différents pour chaque type de billet. Pour cela il faut le plugin de liaison [Billetteries nominatives par profils](https://git.spip.net/spip-contrib-extensions/billetteries_profils).

Lors de l'édition des types de billet, il est alors possible de définir pour chacun quel profil d'informations devra être demandé pour la personne correspondante.

## Exemples d'utilisation
TODO