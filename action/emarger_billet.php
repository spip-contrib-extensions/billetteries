<?php
/**
 * Indique si un billet a été utilisé ou pas.
 *
 * @example
 * #URL_ACTION_AUTEUR{emarger_billet,#ID_BILLET-0}
 * #URL_ACTION_AUTEUR{emarger_billet,#ID_BILLET-1}
 * 
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_emarger_billet_dist($arg = null) {
	if (is_null($arg)) {
		// DEMI sécurité : s'il y a un hash, on teste la sécurité
		if (_request('hash')) {
			$securiser_action = charger_fonction('securiser_action', 'inc');
			$arg = $securiser_action();
		} else {
			// Sinon, on prend l'arg direct
			$arg = _request('arg');
		}
	}

	// Argument de la forme "<id_billet>-0" ou "<id_billet>-1"
	list($id_billet, $emargement) = explode('-', $arg);

	// Il faut pouvoir modifier le billet
	if (
		$id_billet = intval($id_billet)
		and autoriser('modifier', 'billet', $id_billet)
		and (is_numeric($emargement))
	) {

		include_spip('action/editer_objet');

		$emargement = intval($emargement);
		$statuts = array(
			0 => 'valide',
			1 => 'utilise',
		);
		$set = array('statut' => $statuts[$emargement]);

		$maj = objet_instituer('billet', $id_billet, $set);
	}
}
