<?php
/**
 * Déplace le rang d'un contenu de sélection
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_deplacer_billets_type_dist($arg = null) {
	if (is_null($arg)) {
		// DEMI sécurité : s'il y a un hash, on teste la sécurité
		if (_request('hash')) {
			$securiser_action = charger_fonction('securiser_action', 'inc');
			$arg = $securiser_action();
		} else {
			// Sinon, on prend l'arg direct
			$arg = _request('arg');
		}
	}

	// Argument de la forme "123-haut" ou "123-bas" ou "123-3" (rang précis)
	list($id_billets_type, $deplacement) = explode('-', $arg);

	// Il faut pouvoir modifier le contenu et que le déplacement soit un truc valide
	if (
		$id_billets_type = intval($id_billets_type)
		and autoriser('modifier', 'billets_type', $id_billets_type)
		and (
			in_array($deplacement, array('haut', 'bas'))
			or ($nouvelle_position = intval($deplacement) and $nouvelle_position >= 1)
		)
	) {
		// On cherche le parent
		$id_parent = sql_getfetsel('id_billetterie', 'spip_billets_types', 'id_billets_type = '.$id_billets_type);

		// On cherche le rang du type de billet en question
		$rang = sql_getfetsel('rang', 'spip_billets_types', 'id_billets_type = '.$id_billets_type);

		// On cherche le premier et dernier rang du même parent
		$premier_rang = sql_getfetsel('rang', 'spip_billets_types', 'id_billetterie = '.$id_parent, '', 'rang', '0,1');
		$dernier_rang = sql_getfetsel('rang', 'spip_billets_types', 'id_billetterie = '.$id_parent, '', 'rang desc', '0,1');

		// On teste maintenant les différents cas
		if ($deplacement === 'bas') {
			// Si c'était tout en bas, on remonte en haut
			if ($rang >= $dernier_rang) {
				$nouveau_rang = $premier_rang;
				// On décale tous les rangs vers le bas
				sql_update(
					'spip_billets_types',
					array('rang' => 'rang + 1'),
					'id_billetterie = '.$id_parent
				);
			} else {
				$nouveau_rang = $rang + 1;
				// On échange avec le contenu qui avait ce rang là
				sql_updateq(
					'spip_billets_types',
					array('rang' => $rang),
					array(
						'id_billetterie = '.$id_parent,
						'rang = '.$nouveau_rang
					)
				);
			}
		} elseif ($deplacement === 'haut') {
			// Si c'était tout en haut, on redescend tout en bas
			if ($rang <= $premier_rang) {
				$nouveau_rang = $dernier_rang;
				$second_rang = sql_getfetsel('rang', 'spip_billets_types', 'id_billetterie = '.$id_parent." AND id_billets_type != ".$id_billets_type, '', 'rang', '0,1');
				if ($second_rang > 0) {
					// On décale tous les rangs vers le haut
					sql_update(
						'spip_billets_types',
						array('rang' => 'rang - 1'),
						'id_billetterie = '.$id_parent
					);
				}
				else {
					$nouveau_rang++;
				}
			} else {
				$nouveau_rang = $rang - 1;
				// On échange avec le contenu qui avait ce rang là
				sql_updateq(
					'spip_billets_types',
					array('rang' => $rang),
					array(
						'id_billetterie = '.$id_parent,
						'rang = '.$nouveau_rang
					)
				);
			}
		} elseif ($nouvelle_position) {
			$nouveau_rang = sql_getfetsel('rang', 'spip_billets_types', 'id_billetterie = '.$id_parent, '', 'rang', ($nouvelle_position-1) . ',1');
			if (empty($nouveau_rang)) {
				$nouveau_rang = $dernier_rang;
			}

			// Si le nouveau rang est inférieur au rang actuel, on décale tous vers le bas entre les deux
			if ($nouveau_rang < $rang) {
				sql_update(
					'spip_billets_types',
					array('rang' => 'rang + 1'),
					array(
						'id_billetterie = '.$id_parent,
						'rang >= '.$nouveau_rang,
						'rang < '.$rang
					)
				);
			} elseif ($nouveau_rang > $rang) {
				// Sinon l'inverse
				sql_update(
					'spip_billets_types',
					array('rang' => 'rang - 1'),
					array(
						'id_billetterie = '.$id_parent,
						'rang <= '.$nouveau_rang,
						'rang > '.$rang
					)
				);
			}
		}

		// On change enfin le nouveau rang maintenant qu'on a déplacé le reste !
		sql_updateq(
			'spip_billets_types',
			array('rang' => $nouveau_rang),
			'id_billets_type = '.$id_billets_type
		);
	}
}