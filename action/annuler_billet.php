<?php
/**
 * Indique si un billet a été utilisé ou pas.
 *
 * @example
 * #URL_ACTION_AUTEUR{emarger_billet,#ID_BILLET-0}
 * #URL_ACTION_AUTEUR{emarger_billet,#ID_BILLET-1}
 * 
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_annuler_billet_dist($arg = null) {
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	// Si on a bien un billet et qu'on a le droit de l'annuler
	if (
		$id_billet = intval($arg)
		and autoriser('annuler', 'billet', $id_billet)
	) {
		include_spip('action/editer_objet');

		$maj = objet_modifier('billet', $id_billet, array('statut'=>'annule'));
	}
}
