<?php
/**
 * Options au chargement du plugin Billetteries
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Associer une billeterie en limitant a 1 seule si c'est la config choisie
 * @param $ajouter
 * @return bool
 */
function action_editer_liens_ajouter_billetteries_evenement_billetterie_dist($ajouter) {
	$objet_lien = "billetterie";
	$ajout_ok = false;
	include_spip('action/editer_liens');

	// si la config une seule billeterie est activee, on commencer par enlever les liens precedents
	include_spip('inc/config');
	$limiter_nb = lire_config('billetteries/limiter_nb_billetteries_liees', '');

	foreach ($ajouter as $k => $v) {
		if ($lien = lien_verifier_action($k, $v)) {
			$ajout_ok = true;
			list($objet1, $ids, $objet2, $idl) = explode('-', $lien);

			$qualifs = lien_retrouver_qualif($objet_lien, $lien);
			if ($objet_lien == $objet1) {
				if ($limiter_nb) {
					// evenement est l'objet 2
					objet_dissocier([$objet1 => '*'], [$objet2 => $idl]);
				}
				lien_ajouter_liaisons($objet1, $ids, $objet2, $idl, $qualifs);
			} else {
				if ($limiter_nb) {
					// evenement est l'objet 1
					objet_dissocier([$objet2 => '*'], [$objet1 => $ids]);
				}
				lien_ajouter_liaisons($objet2, $idl, $objet1, $ids, $qualifs);
			}
			set_request('id_lien_ajoute', $ids);
		}
	}

	return $ajout_ok;
}