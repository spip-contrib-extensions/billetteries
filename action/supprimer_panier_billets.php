<?php
/**
 * Supprime les billets panier du visiteur en cours et possiblement sa commande en cours
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_supprimer_panier_billets_dist() {
	include_spip('billetteries_fonctions');
	billetteries_supprimer_panier_visiteur();
}
