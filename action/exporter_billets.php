<?php
/**
 * Déplace le rang d'un contenu de sélection
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_exporter_billets_dist($arg = null) {
	if (is_null($arg)) {
		// DEMI sécurité : s'il y a un hash, on teste la sécurité
		if (_request('hash')) {
			$securiser_action = charger_fonction('securiser_action', 'inc');
			$arg = $securiser_action();
		} else {
			// Sinon, on prend l'arg direct
			$arg = _request('arg');
		}
	}
	
	if (
		$id_billets_type = intval($arg)
		and $billets_type = sql_fetsel('*', 'spip_billets_types', 'id_billets_type = '.$id_billets_type)
	) {
		include_spip('inc/charsets');
		
		$colonnes = array();
		$donnees = array();
		
		// Colonnes de base
		$colonnes = array(
			_T('billet:champ_code_label'),
			_T('billet:champ_id_auteur_acheteur_label'),
			_T('billet:champ_id_auteur_label'),
		);
		// On passe dans un pipeline pour pouvoir ajouter des colonnes
		$colonnes = pipeline(
			'billetteries_exporter_billets_colonnes',
			array(
				'args' => array('id_billets_type'=>$id_billets_type, 'billets_type' => $billets_type),
				'data' => $colonnes,
			)
		);
		
		// On récupère tous les billets VALIDES de ce type
		if ($billets = sql_allfetsel('*', 'spip_billets', array('id_billets_type = '.$id_billets_type, 'statut = "valide"'))) {
			include_spip('inc/filtres');
			
			foreach ($billets as $billet) {
				// Données de base
				$ligne = array(
					$billet['code'],
					$billet['id_auteur_acheteur'] ? generer_info_entite($billet['id_auteur_acheteur'], 'auteur', 'titre') . " (${billet['id_auteur_acheteur']})" : '',
					$billet['id_auteur'] ? generer_info_entite($billet['id_auteur'], 'auteur', 'titre') . " (${billet['id_auteur']})" : '',
				);
				// On passe dans un pipeline pour pouvoir ajouter des colonnes
				$ligne = pipeline(
					'billetteries_exporter_billets_ligne',
					array(
						'args' => array('id_billets_type' => $id_billets_type, 'billets_type' => $billets_type, 'billet' => $billet),
						'data' => $ligne,
					)
				);
				
				$donnees[] = $ligne;
			}
		}
		
		$fichier = translitteration($billets_type['titre']);
		$exporter_csv = charger_fonction('exporter_csv', 'inc/');
		$exporter_csv($fichier, $donnees, ',', $colonnes);
	}
}
