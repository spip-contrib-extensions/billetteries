<?php
/**
 * Utilisation de l'action supprimer pour l'objet billets_type
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}



/**
 * Action pour supprimer un·e billets_type
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 *
 * @param null|int $arg
 *     Identifiant à supprimer.
 *     En absence de id utilise l'argument de l'action sécurisée.
**/
function action_supprimer_billets_type_dist($arg=null) {
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}
	$id_billets_type = intval($arg);

	// cas suppression
	if ($id_billets_type) {
		sql_delete('spip_billets_types', 'id_billets_type=' . $id_billets_type);
	}
	else {
		spip_log("action_supprimer_billets_type_dist $id_billets_type pas compris");
	}
}
