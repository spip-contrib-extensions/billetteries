<?php
/**
 * Fonctions utiles au plugin Billetteries
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * critere {orphelins} selectionne les billetteries sans liens avec un objet éditorial
 *
 * @param string $idb
 * @param object $boucles
 * @param object $crit
 */
function critere_BILLETTERIES_orphelins_dist($idb, &$boucles, $crit) {
	$boucle = &$boucles[$idb];
	$cond = $crit->cond;
	$not = $crit->not ? '' : 'NOT';

	$select = sql_get_select('DISTINCT id_billetterie', 'spip_billetteries_liens as oooo');
	$where = "'".$boucle->id_table.".id_billetterie $not IN (SELECT * FROM($select) AS subquery)'";
	if ($cond) {
		$_quoi = '@$Pile[0]["orphelins"]';
		$where = "($_quoi)?$where:''";
	}

	$boucle->where[]= $where;
}

/**
 * Retourne du vide pour les dates "vides"
 *
 * @param string $date
 * @param string
 */
function filtrer_date_vide($date) {
	//if (!intval(preg_replace('/[^\d]/', '', $date))) {
	if ($date == '0000-00-00 00:00:00') {
		$date = null;
	}
	return $date;
}

/**
 * Tester la disponibilité d'une billetterie
 *
 * Disponibilté = dates d'ouverture + quota si les listes d'attentes ne sont pas autorisées.
 *
 * @uses billetteries_tester_dates_ouverture()
 * @uses billetteries_compter_billets_disponibles()
 *
 * @param int $id_billetterie
 *      n° d'une billetterie
 * @return bool
 */
function billetteries_tester_disponibilite($id_billetterie) {

	$autoriser_liste_attente = (intval(sql_getfetsel('liste_attente', 'spip_billetteries', 'id_billetterie='.intval($id_billetterie))) !== 0);
	$test_ouverture = billetteries_tester_dates_ouverture($id_billetterie);
	$test_billets_disponibles = billetteries_compter_billets_disponibles($id_billetterie);
	$test_billets_types = billetteries_tester_billets_types($id_billetterie);

	$disponible = (
		$test_ouverture
		and $test_billets_types
		and ($test_billets_disponibles or $autoriser_liste_attente)
	);

	return $disponible;
}


/**
 * Tester si une billetterie ou un type de billet est ouvert à la vente en fonction des dates de début et de fin.
 * S'il s'agit d'un type de billet, on prend les dates de la billetterie parente en fallback.
 *
 * @param int $id_objet
 *      n° d'une billetterie ou d'un type de billet
 * @param string $objet
 *      billetterie | billets_type
 * @return bool
 */
function billetteries_tester_dates_ouverture($id_objet, $objet = 'billetterie') {
	static $ouvertures;

	if (isset($ouvertures[$objet][$id_objet])) {
		return $ouvertures[$objet][$id_objet];
	}

	// Partons du principe que c'est ok, on regarde s'il faut invalider
	$ouverture = true;

	// Retrouver les dates configurées
	include_spip('base/objets');
	$table_objet     = table_objet_sql($objet);
	$cle_objet       = id_table_objet($objet);
	$select          = array('date_debut', 'date_fin');
	$is_billets_type = (objet_type($objet) == 'billets_type');
	if ($is_billets_type) {
		$select[] = 'id_billetterie';
	}
	$res = sql_fetsel($select, $table_objet, $cle_objet.'='.intval($id_objet));
	$date_debut = filtrer_date_vide($res['date_debut']);
	$date_fin   = filtrer_date_vide($res['date_fin']);
	$time = time();

	// Si type de billet, fallback sur la billetterie parente
	if (
		$is_billets_type 
		&& ($billetterie = sql_fetsel('date_debut, date_fin', 'spip_billetteries', 'id_billetterie='.intval($res['id_billetterie'])))
	) {
		$date_debut = $date_debut ? $date_debut : filtrer_date_vide($billetterie['date_debut']);
		$date_fin = $date_fin ? $date_fin : filtrer_date_vide($billetterie['date_fin']);
	}

	if (
		// Avant la date d'ouverture
		(
			$date_debut
			and $time < strtotime($date_debut)
		)
		// Après la date de fermeture
		or (
			$date_fin
			and $time > strtotime($date_fin)
		)
	) {
		$ouverture = false;
	}

	$ouvertures[$objet][$id_objet] = $ouverture;

	return $ouverture;
}

/**
 * Teste si une billetterie a tout bêtement des types de billets, auquel cas on ne peut pas faire grand chose avec.
 *
 * @param integer $id_billetterie
 * @return bool
 */
function billetteries_tester_billets_types($id_billetterie) {
	static $billetteries = array();

	if (isset($billetteries[$id_billetterie])) {
		return $billetteries[$id_billetterie];
	}

	$is_billets_type = (intval(sql_countsel('spip_billets_types', 'id_billetterie='.intval($id_billetterie))) > 0);
	$billetteries[$id_billetterie] = $is_billets_type;

	return $is_billets_type;
}

/**
 * Renvoyer le nombre de billets disponibles restants pour une billetterie ou un type de billet
 *
 * @param int $id_objet
 *      n° d'une billetterie ou d'un type de billet
 * @param string $objet
 *      Type d'objet : billetterie | billets_type
 * @return int|bool
 * 		Retourne le nombre de billets possibles à acheter, ou true si pas de limite
 */
function billetteries_compter_billets_disponibles($id_objet, $objet = 'billetterie') {
	static $dispos = array();

	if (isset($dispos[$objet][$id_objet])) {
		return $dispos[$objet][$id_objet];
	}

	// Par défaut c'est infini sans quota
	include_spip('base/objets');
	$dispo = true;
	$quota = 0;
	$table_objet = table_objet_sql($objet);
	$cle_objet = id_table_objet($objet);
	$is_billets_type = (objet_type($objet) == 'billets_type');
	$is_billetterie = (objet_type($objet) == 'billetterie');

	// Avant tout, si billetterie, on regarde s'il y a des types de billets
	if (
		$is_billetterie
		and !billetteries_tester_billets_types($id_objet)
	) {
		$dispo = 0;
	}

	// On regarde s'il y a des quotas
	$select = array('quota');
	if ($is_billets_type) {
		$select[] = 'id_billetterie';
	}
	$res = sql_fetsel($select, $table_objet, $cle_objet.'='.intval($id_objet));
	$quota = intval($res['quota']);

	// On compare avec le nombre de billets déjà pris s'il y a un quota seulement
	if (
		$dispo
		and $quota
	) {
		$nb_billets = billetteries_compter_billets($id_objet, $objet);
		$dispo = max($quota - $nb_billets, 0);
	}

	// Si type de billets, on teste aussi toujours la billetterie parente
	// et on prend le plus petit résultat des deux !
	if (
		$is_billets_type
		and $id_billetterie = intval($res['id_billetterie'])
		and $quota_billetterie = intval(sql_getfetsel('quota', 'spip_billetteries', 'id_billetterie='.$id_billetterie))
	) {
		$nb_billets_billetterie = billetteries_compter_billets($id_billetterie, 'billetterie');
		$dispo_billetterie = max($quota_billetterie - $nb_billets_billetterie, 0);

		// Si la dispo de la billetterie est plus petite que la dispo du type de billet, c'est prioritaire
		if ($dispo === true or $dispo_billetterie < $dispo) {
			$dispo = $dispo_billetterie;
		}
	}

	$dispos[$objet][$id_objet] = $dispo;

	return $dispo;
}
/**
 * Compter les billets utilises, pour connaitre le nombre de personnes present dans le spectacle
 *
 * @param int $id_objet
 *      n° d'une billetterie ou d'un type de billet
 * @param string $objet
 *      Type d'objet : billetterie | billets_type
 * @return int
 */
function billetteries_compter_billets_utilises($id_objet, $objet = 'billetterie') {
	static $billets;

	if (isset($billets[$objet][$id_objet])) {
		return $billets[$objet][$id_objet];
	}

	$is_billets_type = (objet_type($objet) == 'billets_type');
	if ($is_billets_type) {
		$ids_billets_type = $id_objet;
	} else {
		$ids_billets_type = array_column(sql_allfetsel('id_billets_type', 'spip_billets_types', 'id_billetterie='.intval($id_objet)), 'id_billets_type');
	}
	$from = array('spip_billets');
	$where = array(
		sql_in('id_billets_type', $ids_billets_type),
		'statut="utilise"',
	);
	$nb_billets = intval(sql_countsel($from, $where));

	$billets[$objet][$id_objet] = $nb_billets;

	return $nb_billets;
}

/**
 * Compter les billets valables (qui bloquent ensuite la vente) d'une billetterie ou d'un type de billets.
 *
 * Ne prend pas en compte les billets annulés, etc (mais liste attente oui car on ne doit pas réouvrir la vente s'il y a des gens en attente)
 * et ne prend pas en compte les billets panier qui ont expiré leur durée
 *
 * @param int $id_objet
 *      n° d'une billetterie ou d'un type de billet
 * @param string $objet
 *      Type d'objet : billetterie | billets_type
 * @return int
 */
function billetteries_compter_billets($id_objet, $objet = 'billetterie') {
	static $billets;
	$duree_panier = intval(lire_config('billetteries/duree_panier', 30));
	$date_maxi = date('Y-m-d H:i:s', time() - 60 * $duree_panier);

	if (isset($billets[$objet][$id_objet])) {
		return $billets[$objet][$id_objet];
	}

	$is_billets_type = (objet_type($objet) == 'billets_type');
	if ($is_billets_type) {
		$ids_billets_type = $id_objet;
	} else {
		$ids_billets_type = array_column(sql_allfetsel('id_billets_type', 'spip_billets_types', 'id_billetterie='.intval($id_objet)), 'id_billets_type');
	}
	$from = array('spip_billets');
	$where = array(
		sql_in('id_billets_type', $ids_billets_type),
		sql_in('statut', array('panier', 'attente', 'valide', 'utilise')),
		'(statut != "panier" OR date >= '.sql_quote($date_maxi).')'
	);
	$nb_billets = intval(sql_countsel($from, $where));

	$billets[$objet][$id_objet] = $nb_billets;

	return $nb_billets;
}

/**
 * Retourne les numéros des types de billets disponibles pour une billetterie
 *
 * C'est à dire :
 * - Les dates d'ouvertures sont ok
 * - Il reste des billets
 *
 * @param int|string $id_billetterie
 * @return array <ids>
 */
function billetteries_ids_billets_types_disponibles($id_billetterie) {
	$ids_billets_types = array();
	$id_billetterie = intval($id_billetterie);

	if (
		$id_billetterie > 0
		and $billets_types = sql_allfetsel('*', 'spip_billets_types', "id_billetterie = $id_billetterie", '', 'rang ASC')
	) {
		// On teste la disponibilité
		foreach($billets_types as $billets_type) {
			$id_billets_type = intval($billets_type['id_billets_type']);
			$billets_type_ouvert = billetteries_tester_dates_ouverture($id_billets_type, 'billets_type'); // bool
			$billets_type_dispo = billetteries_compter_billets_disponibles($id_billets_type, 'billets_type'); // bool ou int
			$est_disponible = ($billets_type_ouvert && $billets_type_dispo);
			if ($est_disponible) {
				$ids_billets_types[] = $id_billets_type;
			}
		}
	}

	return $ids_billets_types;
}

/**
 * Affiche un intervalle de dates, une seule date ou rien.
 *
 * Version améliorée de la fonction de spip :
 * - Gère les cas où on n'a aucune ou une seule date.
 * - Prend en compte si les dates sont dans le passé, en cours ou dans le futur.
 *
 * @example
 * Du x au y
 * À partir du x / Depuis le x
 * Jusqu'au y
 *
 * @uses affdate_debut_fin
 *
 * @param string $date_debut
 * @param string $date_fin
 * @param string $horaire
 * @param string $forme abbr|annee|jour|hcal
 * @return string
 */
function affdate_debut_fin_billetterie($date_debut, $date_fin, $horaire = 'oui', $forme = '') {
	$date = '';
	$time       = time();
	$time_debut = 0;
	$time_fin   = 10**10;

	if ($date_debut = filtrer_date_vide($date_debut)) {
		$time_debut = strtotime($date_debut);
	}
	if ($date_fin = filtrer_date_vide($date_fin)) {
		$time_fin = strtotime($date_fin);
	}
	$futur    = ($time < $time_debut);
	$passe    = ($time > $time_fin);
	$en_cours = (($time >= $time_debut) and ($time <= $time_fin));

	// 1) Il y a 2 dates
	if (
		$date_debut
		and $date_fin
	) {
		// Futur ou passé : « du x au y »
		if (
			$futur
			or $passe
		) {
			$date = affdate_debut_fin($date_debut, $date_fin, $horaire, $forme);
		// En cours : « jusqu'au x »
		} elseif ($en_cours) {
			$date = _T('billetterie:texte_date_jusquau', array('date' => affdate($date_fin)));
		}

	// 2) Uniquement la date de début
	} elseif (
		$date_debut
		and !$date_fin
	) {
		// Futur : « à partir du x »
		if ($futur) {
			$date = _T('billetterie:texte_date_a_partir_du', array('date' => affdate($date_debut)));
			// En cours ou passé : « depuis le x »
		}/* elseif (
			$en_cours
			or $passe
		) {
			$date = _T('billetterie:texte_date_depuis', array('date' => affdate($date_debut)));
		}
		*/

	// 3) Uniquement la date de fin : « jusqu'au x »
	} elseif (
		!$date_debut
		and $date_fin
	) {
		$date = _T('billetterie:texte_date_jusquau', array('date' => affdate($date_fin)));
	}

	return $date;
}

/**
 * Nettoyer tous les paniers (et commande-panier) trop vieux, de qui que ce soit
 *
 **/
function billetteries_nettoyer_paniers() {
	include_spip('inc/config');
	include_spip('action/editer_objet');
	include_spip('inc/autoriser');

	if ($duree_panier = intval(lire_config('billetteries/duree_panier', 30))) {
		$date_maxi = date('Y-m-d H:i:s', time() - 60 * $duree_panier);

		// On abandonne les billets panier trop vieux
		if ($billets = sql_allfetsel(
			'id_billet',
			'spip_billets',
			array(
				'statut = "panier"',
				'date < '.sql_quote($date_maxi),
			)
		)) {
			$ids_billets = array_column($billets, 'id_billet');

			// Pour chacun de ses billets on le met en statut "abandonne"
			foreach ($ids_billets as $id_billet) {
				autoriser_exception('modifier', 'billet', $id_billet, true);
				autoriser_exception('instituer', 'billet', $id_billet, true);
				objet_modifier('billet', $id_billet, array(
					'statut' => 'abandonne',
				));
				autoriser_exception('instituer', 'billet', $id_billet, false);
				autoriser_exception('modifier', 'billet', $id_billet, false);
			}
			$texte_billets = implode(', ', $ids_billets);
			spip_log("[nettoyer_panier] Abandon des billets trop vieux : $texte_billets (âge maxi {$duree_panier}mn)", 'billetteries' . _LOG_INFO);
		}

		// On abandonne les commandes en cours trop vieilles si ya le plugin
		if (
			defined('_DIR_PLUGIN_COMMANDES')
			and $commandes = sql_allfetsel(
				'id_commande',
				'spip_commandes',
				array(
					'statut = "encours"',
					'date < '.sql_quote($date_maxi),
				)
			)
			and $ids_commandes = array_column($commandes, 'id_commande')
		) {
			// On passe chacune en "abandonne"
			foreach ($ids_commandes as $id_commande) {
				autoriser_exception('modifier', 'commande', $id_commande, true);
				autoriser_exception('instituer', 'commande', $id_commande, true);
				objet_modifier('commande', $id_commande, array(
					'statut' => 'abandonne',
				));
				autoriser_exception('instituer', 'commande', $id_commande, false);
				autoriser_exception('modifier', 'commande', $id_commande, false);
			}
			$texte_commandes = implode(', ', $ids_commandes);
			spip_log("[nettoyer_panier] Abandon des commandes trop vieilles : $texte_commandes (âge maxi {$duree_panier}mn)", 'billetteries' . _LOG_INFO);
		}
	}
}

/**
 * Lister les billets qui sont dans le panier d'un visiteur
 *
 * @return array
 * 		Retourne une liste des id_billet
 **/
function billetteries_panier_visiteur() {
	include_spip('inc/session');

	static $ids_billets = null;

	if (is_null($ids_billets)) {
		billetteries_nettoyer_paniers();
		$ids_billets = array();
		$where = array();

		// On cherche un cookie unique
		$nom_cookie = $GLOBALS['cookie_prefix'] . '_panier_billetteries';
		if (isset($_COOKIE[$nom_cookie]) and $cookie = $_COOKIE[$nom_cookie]) {
			$where[] = 'cookie = '.sql_quote($cookie);
		}

		// On cherche aussi si id_auteur connecté
		if ($id_auteur = intval(session_get('id_auteur')) and $id_auteur > 0) {
			$where[] = 'id_auteur_acheteur = '.$id_auteur;
		}

		if ($where) {
			$where = implode(' or ', $where);

			if ($billets = sql_allfetsel(
				'id_billet',
				'spip_billets',
				array('statut = "panier"', "($where)")
			)) {
				$ids_billets = array_column($billets, 'id_billet');
			}
		}
	}

	return $ids_billets;
}

function billetteries_supprimer_panier_visiteur() {
	include_spip('action/editer_liens');
	include_spip('action/editer_objet');
	include_spip('inc/autoriser');
	$where = array();

	// On cherche un cookie unique
	$nom_cookie = $GLOBALS['cookie_prefix'] . '_panier_billetteries';
	if (isset($_COOKIE[$nom_cookie]) and $cookie = $_COOKIE[$nom_cookie]) {
		$where[] = 'cookie = '.sql_quote($cookie);
	}

	// On cherche aussi si id_auteur connecté
	if ($id_auteur = intval(session_get('id_auteur')) and $id_auteur > 0) {
		$where[] = 'id_auteur_acheteur = '.$id_auteur;
	}

	if ($where) {
		$where = implode(' or ', $where);

		// Les billets en panier
		if ($billets = sql_allfetsel(
			'id_billet',
			'spip_billets',
			array(
				'statut = "panier"',
				"($where)",
			)
		)) {
			$ids_billets = array_column($billets, 'id_billet');

			// Pour chacun de ses billets on le met en statut "abandonne"
			foreach ($ids_billets as $id_billet) {
				autoriser_exception('modifier', 'billet', $id_billet, true);
				autoriser_exception('instituer', 'billet', $id_billet, true);
				objet_modifier('billet', $id_billet, array(
					'statut' => 'abandonne',
				));
				autoriser_exception('instituer', 'billet', $id_billet, false);
				autoriser_exception('modifier', 'billet', $id_billet, false);
			}
		}


		// La commande si ya le plugin et qu'il y a un auteur connecté
		if (
			defined('_DIR_PLUGIN_COMMANDES')
			and $commandes = sql_allfetsel(
				'id_commande',
				'spip_commandes',
				array(
					'statut = "encours"',
					'id_auteur = '.$id_auteur,
				)
			)
			and $ids_commandes = array_column($commandes, 'id_commande')
		) {
			// On passe chacune en "abandonne"
			foreach ($ids_commandes as $id_commande) {
				autoriser_exception('modifier', 'commande', $id_commande, true);
				autoriser_exception('instituer', 'commande', $id_commande, true);
				objet_modifier('commande', $id_commande, array(
					'statut' => 'abandonne',
				));
				autoriser_exception('instituer', 'commande', $id_commande, false);
				autoriser_exception('modifier', 'commande', $id_commande, false);
			}
		}
	}
}

/**
 * Critère pour prendre les billets présents dans le panier du visiteur
 *
 * @uses billetteries_panier_visiteur()
 * @example <BOUCLE_billets(BILLETS) {panier_visiteur}>
 *
 * @param string $idb
 * @param object $boucles
 * @param object $crit
 */
function critere_BILLETS_panier_visiteur_dist($idb, &$boucles, $crit) {
	$boucle = &$boucles[$idb];
	$cond = $crit->cond;
	$not = $crit->not ? 'NOT' : '';
	$id_table = $boucle->id_table; // nom ou alias de la table
	$where = "sql_in('$id_table.id_billet', billetteries_panier_visiteur(), $not)";
	$boucle->where[]= $where;
	$boucles[$idb]->descr['session'] = true; // drapeau pour avoir un cache visiteur
}

/**
 * Renvoie le numéro de la commande en cours du visiteur contenant les billets mis au panier
 *
 * On va chercher la commande liée aux billets du panier.
 * On vérifie en plus :
 *
 * - Qu'elle est en cours, pas abandonnée ou autre.
 * - Que les billets sont dans une commande unique.
 *   (le cas inverse n'est pas censé se produire, il faut alors refaire la commande dans la fonction appelante).
 *
 * @param array $ids_billets_panier
 * @return int
 *    `0` si aucune commande valide n'est trouvée
 */
function billetteries_id_commande_encours_visiteur($ids_billets_panier = array()) {
	include_spip('base/abstract_sql');
	include_spip('action/editer_liens');

	$id_commande = 0;
	if (!$ids_billets_panier) {
		include_spip('billetteries_fonctions');
		$ids_billets_panier = billetteries_panier_visiteur();
	}

	if (
		$ids_billets_panier
		and $liens = objet_trouver_liens(array('commande' => '*'), array('billet' => $ids_billets_panier))
	) {
		if (function_exists('array_column')) {
			$ids_commandes = array_column($liens, 'id_commande');
		} else {
			$ids_commandes = array_map('reset', $liens); // array_column à partir de spip 4
		}
		$ids_commandes = array_unique($ids_commandes);
		// Les billets au panier doivent se trouver dans une commande unique
		if (count($ids_commandes) > 1) {
			spip_log('[id_commande_encours_visiteur] Invalide, les billets du panier sont liés à plusieurs commandes : ' . implode(',', $ids_commandes), 'billetteries' . LOG_INFO);
		} else {
			include_spip('commandes_fonctions');
			$id_commande_liee = intval($ids_commandes[0]);
			$id_commande_visiteur = commandes_id_commande_encours_visiteur();
			// Il faut qu'elle soit en cours et celle du visiteur
			if ($id_commande_liee !== $id_commande_visiteur) {
				spip_log("[id_commande_encours_visiteur] Invalide, les billets du panier sont lié à une commande pas en cours ou différente de celle du visiteur : commande panier = $id_commande_liee / commande visiteur = $id_commande_visiteur", 'billetteries' . LOG_INFO);
			// Tout va bien
			} else {
				$id_commande = $id_commande_liee;
			}
		}
	}

	return $id_commande;
}
