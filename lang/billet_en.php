<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_billet' => 'Add this ticket',

	// B
	'bouton_enregistrer_billet' => 'Save this ticket',
	'bouton_enregistrer_billet_continuer' => 'Save this thicket and continue',

	// C
	'champ_code_label' => 'Code',
	'champ_email_label' => 'Email',
	'champ_id_auteur_label' => 'Ticket owner',
	'champ_id_auteur_acheteur_label' => 'Ticket buyer',
	'champ_infos_label' => 'Miscellaneous information',
	'confirmer_supprimer_billet' => 'Are you sure you want to delete this ticket?',

	// I
	'icone_creer_billet' => 'Create a ticket',
	'icone_modifier_billet' => 'Edit this ticket',
	'info_1_billet' => 'One ticket',
	'info_aucun_billet' => 'No tickets',
	'info_billets_auteur' => 'Tickets of this user',
	'info_nb_billets' => '@nb@ tickets',

	// R
	'retirer_lien_billet' => 'Remove this ticket',
	'retirer_tous_liens_billets' => 'Remove all tickets tickets',

	// S
	'supprimer_billet' => 'Delete this ticket',

	// T
	'texte_ajouter_billet' => 'Add a ticket',
	'texte_changer_statut_billet' => 'This ticket is:',
	'texte_creer_associer_billet' => 'Create and link a ticket',
	'texte_definir_comme_traduction_billet' => 'This ticket is a translation of ticket number:',
	'texte_statut_attente' => 'Pending list',
	'texte_statut_panier' => 'Cart',
	'texte_statut_valide' => 'Validated',
	'texte_statut_utilise' => 'Used',
	'texte_statut_annule' => 'Canceled',
	'texte_statut_abandonne' => 'Abandonned',
	'titre_billet' => 'Ticket',
	'titre_billet_commande' => 'Ticket from the order @commande@',
	'titre_billets' => 'Tickets',
	'titre_billets_commande' => 'Tickets from the order @commande@',
	'titre_billets_publies' => 'Validated tickets',
	'titre_billets_rubrique' => 'Tickets of the section',
	'titre_langue_billet' => 'This ticket language',
	'titre_logo_billet' => 'This ticket logo',
	'titre_objets_lies_billet' => 'Linked to this ticket',
);
