<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_billetterie' => 'Add this ticket office',

	// B
	'billetterie_vue' => 'Ticket office seen in a text',
	'bouton_commander' => 'Order',
	'bouton_details' => 'Details',
	'bouton_choisir_billet' => 'Select this ticket',

	// C
	'champ_date_debut_label' => 'Opening day',
	'champ_date_fin_label' => 'Closing day',
	'champ_descriptif_label' => 'Description',
	'champ_quota_explication' => 'Maximal number of tickets',
	'champ_quota_label' => 'Quota',
	'champ_quota_aucun' => 'No one',
	'champ_titre_label' => 'Title',
	'champ_liste_attente_label_case' => 'Allow pending list',
	'champ_quantite_label' => 'Quantity',
	'champ_taxe_label' => 'Tax',
	'champ_total_label' => 'Total',
	'confirmer_supprimer_billetterie' => 'Are you sure you want to delete this ticket office?',

	// M
	'menu_login' => 'I already have an account',
	'menu_signup' => 'Create an account',
	'message_billetterie_indisponible' => 'This ticket office is not open for booking.',

	// I
	'icone_creer_billetterie' => 'Create a ticket office',
	'icone_modifier_billetterie' => 'Edit this ticket office',
	'info_1_billetterie' => 'One ticket office',
	'info_1_utilisation' => 'One use',
	'info_aucun_billetterie' => 'No ticket office',
	'info_aucune_utilisation' => 'This ticket office is not used in any content.',
	'info_billetteries_auteur' => 'Ticket offices of this user',
	'info_nb_billetteries' => '@nb@ ticket offices',
	'info_nb_dispo' => '@nb@ available tickets left.',
	'info_nb_utilisations' => '@nb@ uses',
	'info_dates_ouverture' => 'Open @date@',

	// R
	'retirer_lien_billetterie' => 'Remove this ticket office',
	'retirer_tous_liens_billetteries' => 'Remove all ticket offices',

	// S
	'supprimer_billetterie' => 'Delete this ticket office',

	// T
	'titre_billetterie' => 'Ticket office',
	'titre_billetteries' => 'Ticket offices',
	'titre_billetteries_autonomes' => 'Autonomous ticket offices',
	'titre_billetteries_objets' => 'Ticket offices linked to contents',
	'titre_billetteries_rubrique' => 'Ticket offices of this section',
	'titre_langue_billetterie' => 'This ticket office language',
	'titre_logo_billetterie' => 'This ticket office logo',
	'titre_objets_lies_billetterie' => 'Linked to this ticket office',
	'titre_billetterie_ouverte' => 'Ticket office open',
	'titre_billets_type_ferme' => 'Not available',
	'texte_ajouter_billetterie' => 'Add a ticket office',
	'texte_changer_statut_billetterie' => 'This ticket office is:',
	'texte_creer_associer_billetterie' => 'Create and link a ticket office',
	'texte_definir_comme_traduction_billetterie' => 'This ticket office is a translation of the ticket office number:',
	'texte_date_a_partir_du' => 'from @date@',
	'texte_date_depuis' => 'since @date@',
	'texte_date_jusquau' => 'until @date@', 
	'texte_statut_actif' => 'Active',
	'texte_statut_inactif' => 'Inactive',
	'texte_billetterie_fermee' => 'This ticket office is closed',

	'utiliser_lien_billetterie' => 'Use this ticket office',

);
