<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_billets_type' => 'Ajouter ce type de billet',

	// C
	'champ_date_debut_label' => 'Ouverture',
	'champ_date_fin_label' => 'Fermeture',
	'champ_date_min_explication' => 'Si remplie, doit être supérieure ou égale au @date@',
	'champ_date_max_explication' => 'Si remplie, doit être inférieure ou égale au @date@',
	'champ_id_billetterie_label' => 'Dans la billetterie :',
	'champ_descriptif_label' => 'Descriptif',
	'champ_prix_label' => 'Prix',
	'champ_prix_ht_label' => 'Prix HT',
	'champ_prix_label' => 'Prix TTC',
	'champ_prix_gratuit' => 'Gratuit',
	'champ_taxe_label' => 'Taxe',
	'champ_taxe_explication' => 'Nombre décimal entre 0 et 1',
	'champ_quota_label' => 'Quota',
	'champ_quota_explication' => 'Nombre maximal de billets',
	'champ_quota_max_explication' => 'Ne peut dépasser @nb@',
	'champ_selection_min_label' => 'Sélection min',
	'champ_selection_min_explication' => 'Nombre minimal de billets sélectionnables',
	'champ_selection_max_label' => 'Sélection max',
	'champ_selection_max_explication' => 'Nombre maximal de billets sélectionnables. Ne peut dépasser le quota. Valeur ajustée à posteriori en fonction du nombre de billets disponibles.',
	'champ_rang_label' => 'Rang',
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_billets_type' => 'Confirmez-vous la suppression de ce type de billet ?',
	
	// D
	'deplacer' => 'Déplacer ce type de billet',
	'deplacer_apres' => 'Déplacer ce type de billet après',
	'deplacer_avant' => 'Déplacer ce type de billet avant',
	
	// E
	'erreur_selection_max_quota' => 'La valeur ne peut dépasser celle du quota',
	'erreur_quota_billetterie' => 'La valeur ne peut dépasser celle de la billetterie : @quota@',
	'exporter_billets_bouton' => 'Télécharger le tableur des billets',
	'exporter_billets_titre' => 'Feuille d’émargement',
	
	// I
	'icone_creer_billets_type' => 'Créer un type de billet',
	'icone_modifier_billets_type' => 'Modifier ce type de billet',
	'info_1_billets_type' => '1 type de billet',
	'info_aucun_billets_type' => 'Aucun type de billet',
	'info_billets_types_auteur' => 'Les types de billets de cet auteur',
	'info_nb_billets_types' => '@nb@ types de billets',

	// R
	'retirer_lien_billets_type' => 'Retirer ce type de billet',
	'retirer_tous_liens_billets_types' => 'Retirer tous les types de billets',

	// S
	'supprimer_billets_type' => 'Supprimer ce type de billet',

	// T
	'texte_ajouter_billets_type' => 'Ajouter un type de billet',
	'texte_changer_statut_billets_type' => 'Ce type de billet est :',
	'texte_creer_associer_billets_type' => 'Créer et associer un type de billet',
	'texte_definir_comme_traduction_billets_type' => 'Ce type de billet est une traduction du type de billet numéro :',
	'titre_billets_type' => 'Type de billet',
	'titre_billets_types' => 'Types de billets',
	'titre_billets_types_rubrique' => 'Types de billets de la rubrique',
	'titre_langue_billets_type' => 'Langue de ce type de billet',
	'titre_logo_billets_type' => 'Logo de ce type de billet',
	'titre_objets_lies_billets_type' => 'Liés à ce type de billet',
);
