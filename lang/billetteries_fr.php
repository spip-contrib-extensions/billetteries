<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'billetteries_titre' => 'Billetteries',

	// C
	'cfg_duree_panier_explication' => 'Durée en minutes avant de supprimer les commandes de billets non encore payées.',
	'cfg_duree_panier_label' => 'Durée du panier',
	'cfg_ids_admins_annulations_label' => 'Admins notifiés pour les annulations',
	'cfg_limiter_nb_billetteries_liees_label' => 'Limiter à une billetterie par contenu',
	'cfg_objets_explication' => 'Sélectionnez les contenus sur lesquels on pourra ajouter des billetteries.',
	'cfg_objets_label' => 'Billetteries sur les contenus :',
	'cfg_notifications_plugin_explication' => 'L’envoi de notifications nécessite le plugin « Notifications avancées »',
	'cfg_taxe_explication' => 'Nombre décimal entre 0 et 1.',
	'cfg_taxe_label' => 'Taxe par défaut',
	'cfg_titre_parametrages' => 'Paramétrages',
	'commander_erreur_billets_type_quantite' => 'La quantité est trop élevée. Il reste @nb@ places pour ce type de billet.',
	'commander_erreur_billetterie_quantite' => 'La quantité est trop élevée. Il reste @nb@ places pour cette billetterie.',
	'commander_erreur_enregistrement' => 'Erreur lors de l’enregistrement des billets à commander. Veuillez recommencer votre commande.',
	'commander_erreur_transformation_panier' => 'Erreur lors de la création de la commande à partir du panier. Veuillez recommencer votre commande.',
	'commander_erreur_quantite_0' => 'Veuillez sélectionner au moins 1 billet.',

	// T
	'titre_page_configurer_billetteries' => 'Configurer Billetteries',
);
