<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_billetterie' => 'Ajouter cette billetterie',

	// B
	'billetterie_vue' => 'Billetterie vue dans un texte',
	'bouton_commander' => 'Commander',
	'bouton_details' => 'Détails',
	'bouton_choisir_billet' => 'Choisir ce billet',

	// C
	'champ_date_debut_label' => 'Ouverture',
	'champ_date_fin_label' => 'Fermeture',
	'champ_descriptif_label' => 'Descriptif',
	'champ_quota_explication' => 'Nombre maximal de billets',
	'champ_quota_label' => 'Quota',
	'champ_quota_aucun' => 'Aucun',
	'champ_titre_label' => 'Titre',
	'champ_liste_attente_label_case' => 'Autoriser les listes d\'attente',
	'champ_quantite_label' => 'Quantité',
	'champ_taxe_label' => 'TVA',
	'champ_total_label' => 'Total',
	'confirmer_supprimer_billetterie' => 'Confirmez-vous la suppression de cette billetterie ?',

	// M
	'menu_login' => 'J’ai déjà un compte.',
	'menu_signup' => 'Créer un compte',
	'message_billetterie_indisponible' => 'Cette billetterie n’est pas ouverte à la réservation.',

	// I
	'icone_creer_billetterie' => 'Créer une billetterie',
	'icone_modifier_billetterie' => 'Modifier cette billetterie',
	'info_1_billetterie' => '1 billetterie',
	'info_1_utilisation' => '1 utilisation',
	'info_aucun_billetterie' => 'Aucune billetterie',
	'info_aucune_utilisation' => 'Cette billetterie n’est liée à aucun contenu.',
	'info_billetteries_auteur' => 'Les billetteries de cet auteur',
	'info_nb_billetteries' => '@nb@ billetteries',
	'info_nb_dispo' => 'Il reste @nb@ billets disponibles.',
	'info_nb_utilisations' => '@nb@ utilisations',
	'info_emargement_billetterie' => 'Feuille d’émargement de la billetterie',
	'info_emargement_billetteries' => 'Feuille d’émargement des billetteries',
	'info_dates_ouverture' => 'Ouvert @date@',

	// R
	'retirer_lien_billetterie' => 'Retirer cette billetterie',
	'retirer_tous_liens_billetteries' => 'Retirer toutes les billetteries',

	// S
	'supprimer_billetterie' => 'Supprimer cette billetterie',

	// T
	'titre_billetterie' => 'Billetterie',
	'titre_billetteries' => 'Billetteries',
	'titre_billetteries_autonomes' => 'Billetteries autonomes',
	'titre_billetteries_objets' => 'Billetteries liées à des contenus',
	'titre_billetteries_rubrique' => 'Billetteries de la rubrique',
	'titre_langue_billetterie' => 'Langue de cette billetterie',
	'titre_logo_billetterie' => 'Logo de cette billetterie',
	'titre_objets_lies_billetterie' => 'Liés à cette billetterie',
	'titre_billetterie_ouverte' => 'Billetterie ouverte',
	'titre_billets_type_ferme' => 'Indisponible',
	'texte_ajouter_billetterie' => 'Ajouter une billetterie',
	'texte_changer_statut_billetterie' => 'Cette billetterie est :',
	'texte_creer_associer_billetterie' => 'Créer et associer une billetterie',
	'texte_definir_comme_traduction_billetterie' => 'Cette billetterie est une traduction de la billetterie numéro :',
	'texte_date_a_partir_du' => 'À partir du @date@',
	'texte_date_depuis' => 'Depuis le @date@',
	'texte_date_jusquau' => 'Jusqu’au @date@', 
	'texte_statut_actif' => 'Active',
	'texte_statut_inactif' => 'Inactive',
	'texte_billetterie_fermee' => 'Cette billetterie est fermée',

	'utiliser_lien_billetterie' => 'Utiliser cette billetterie',
	
);
