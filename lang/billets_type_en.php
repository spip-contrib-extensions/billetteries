<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_billets_type' => 'Add this ticket type',

	// C
	'champ_date_debut_label' => 'Opening day',
	'champ_date_fin_label' => 'Closing day',
	'champ_date_min_explication' => 'If filled, must be greater than or equal to @date@',
	'champ_date_max_explication' => 'If filled, must be lesser than or equal to @date@',
	'champ_id_billetterie_label' => 'In the ticket office:',
	'champ_descriptif_label' => 'Description',
	'champ_prix_label' => 'Prix',
	'champ_prix_ht_label' => 'No-tax price',
	'champ_prix_label' => 'Tax incl. price',
	'champ_prix_gratuit' => 'Free',
	'champ_taxe_label' => 'Tax',
	'champ_taxe_explication' => 'Decimal between 0 and 1',
	'champ_quota_label' => 'Quota',
	'champ_quota_explication' => 'Maximal number of tickets',
	'champ_quota_max_explication' => 'Can’t exceed @nb@',
	'champ_selection_min_label' => 'Min selection',
	'champ_selection_min_explication' => 'Minimal number of selectable tickets',
	'champ_selection_max_label' => 'Max selection',
	'champ_selection_max_explication' => 'Maximal number of selectable tickets. Can’t exceed the quota. Value will be ajusted according to available tickets.',
	'champ_rang_label' => 'Rank',
	'champ_titre_label' => 'Title',
	'confirmer_supprimer_billets_type' => 'Are you sure you want to delete this ticket type?',
	
	// D
	'deplacer' => 'Move this ticket type',
	'deplacer_apres' => 'Move this ticket type after',
	'deplacer_avant' => 'Move this ticket type before',
	
	// E
	'erreur_selection_max_quota' => 'Value can’t exceed the quota',
	'erreur_quota_billetterie' => 'Value can’t exceed the ticket office quota: @quota@',
	'exporter_billets_bouton' => 'Download tickets spreadsheet',
	'exporter_billets_titre' => 'Sheet of registration',
	
	// I
	'icone_creer_billets_type' => 'Create a ticket type',
	'icone_modifier_billets_type' => 'Edit this ticket type',
	'info_1_billets_type' => 'One ticket type',
	'info_aucun_billets_type' => 'No ticket types',
	'info_billets_types_auteur' => 'Ticket types of this user',
	'info_nb_billets_types' => '@nb@ ticket types',

	// R
	'retirer_lien_billets_type' => 'Remove this ticket types',
	'retirer_tous_liens_billets_types' => 'Remove all ticket types',

	// S
	'supprimer_billets_type' => 'Delete this ticket types',

	// T
	'texte_ajouter_billets_type' => 'Add a ticket type',
	'texte_changer_statut_billets_type' => 'This ticket type is:',
	'texte_creer_associer_billets_type' => 'Create and link a ticket type',
	'texte_definir_comme_traduction_billets_type' => 'This ticket type is a translation of the ticket type number:',
	'titre_billets_type' => 'Ticket type',
	'titre_billets_types' => 'Ticket types',
	'titre_billets_types_rubrique' => 'Ticket types of the section',
	'titre_langue_billets_type' => 'This ticket type language',
	'titre_logo_billets_type' => 'This ticket type logo',
	'titre_objets_lies_billets_type' => 'Linked to this ticket type',
);
