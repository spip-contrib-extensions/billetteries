<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_billet' => 'Ajouter ce billet',

	// B
	'bouton_enregistrer_billet' => 'Enregistrer ce billet',
	'bouton_enregistrer_billet_continuer' => 'Enregistrer ce billet et continuer',

	// C
	'champ_code_label' => 'Code',
	'champ_code_explication' => 'Code unique pour identifier le billet.',
	'champ_email_label' => 'Email',
	'champ_id_auteur_label' => 'Compte détenteur du billet',
	'champ_id_auteur_acheteur_label' => 'Compte ayant acheté le billet',
	'champ_infos_label' => 'Informations complémentaires',
	'champ_infos_explication' => 'Informations de contact, prix du billet, etc. <br />Ce champ ne doit pas être rempli manuellement : il est en lecture seule. Les données sont transposées au format JSON pour une meilleure lisibilité.',
	'champ_infos_billets_type_label' => 'Type de billet',
	'champ_infos_prix_ht_label' => 'Prix HT',
	'champ_infos_taxes_label' => 'Montant des taxes',
	'champ_source_label' => 'Source',
	'champ_source_explication' => 'Indication relative à l’origine du billet.',
	'champ_vide_explication' => 'Laissez vide pour remplir automatiquement à posteriori.',
	'champ_email_vide_explication' => 'Laissez vide pour remplir automatiquement à posteriori si un compte détenteur est sélectionné.',
	'confirmer_supprimer_billet' => 'Confirmez-vous la suppression de cet billet ?',

	// I
	'icone_creer_billet' => 'Créer un billet',
	'icone_modifier_billet' => 'Modifier ce billet',
	'info_billets_auteur' => 'Les billets de cet auteur',
	'info_1_billet' => 'Un billet',
	'info_aucun_billet' => 'Aucun billet',
	'info_nb_billets' => '@nb@ billets',
	'info_1_billet_valide' => 'Un billet validé',
	'info_aucun_billet_valide' => 'Aucun billet validé',
	'info_nb_billets_valides' => '@nb@ billets validés',
	'info_1_billet_autre' => 'Un autre billet',
	'info_aucun_billet_autre' => 'Aucun autre billet',
	'info_nb_billets_autres' => '@nb@ autres billets',

	// R
	'retirer_lien_billet' => 'Retirer ce billet',
	'retirer_tous_liens_billets' => 'Retirer tous les billets',

	// S
	'supprimer_billet' => 'Supprimer cet billet',

	// T
	'texte_ajouter_billet' => 'Ajouter un billet',
	'texte_changer_statut_billet' => 'Ce billet est :',
	'texte_creer_associer_billet' => 'Créer et associer un billet',
	'texte_definir_comme_traduction_billet' => 'Ce billet est une traduction du billet numéro :',
	'texte_statut_attente' => 'En liste d\'attente',
	'texte_statut_panier' => 'Mis au panier',
	'texte_statut_valide' => 'Validé',
	'texte_statut_utilise' => 'Utilisé',
	'texte_statut_annule' => 'Annulé',
	'texte_statut_abandonne' => 'Abandonné',
	'titre_billet' => 'Billet',
	'titre_billet_commande' => 'Billet de la commande @commande@',
	'titre_billets' => 'Billets',
	'titre_billets_valides' => 'Billets validés',
	'titre_billets_autres' => 'Autres billets',
	'titre_billets_commande' => 'Billets de la commande @commande@',
	'titre_billets_publies' => 'Billets validés',
	'titre_billets_rubrique' => 'Billets de la rubrique',
	'titre_langue_billet' => 'Langue de ce billet',
	'titre_logo_billet' => 'Logo de ce billet',
	'titre_objets_lies_billet' => 'Liés à ce billet',
);
