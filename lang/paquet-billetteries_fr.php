<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'billetteries_description' => 'Ce plugin permet la gestion complète d’une ou plusieurs billetteries dans lesquelles on propose des types de billets à acheter. Il est alors possible de commander des billets, grâce aux plugins Commandes et Bank.',
	'billetteries_nom' => 'Billetteries',
	'billetteries_slogan' => 'Gestion de billetteries',
);
