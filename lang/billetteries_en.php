<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'billetteries_titre' => 'Ticket offices',

	// C
	'cfg_duree_panier_explication' => 'Duration in minutes before delete non-paid tickets orders.',
	'cfg_duree_panier_label' => 'Cart duration',
	'cfg_ids_admins_annulations_label' => 'Notified admins for cancellations',
	'cfg_limiter_nb_billetteries_liees_label' => 'Limit to one ticket office max per content',
	'cfg_objets_explication' => 'Select contents on which we can add ticket offices.',
	'cfg_objets_label' => 'Ticket offices on those concents:',
	'cfg_taxe_explication' => 'Decimal number between 0 and 1.',
	'cfg_taxe_label' => 'Default tax',
	'cfg_titre_parametrages' => 'Parameters',
	'commander_erreur_billets_type_quantite' => 'Quantity is too high. It remains @nb@ places for this ticket type.',
	'commander_erreur_billetterie_quantite' => 'Quantity is too high. It remains @nb@ places for this ticket office.',
	'commander_erreur_enregistrement' => 'Error during tickets registration. Please repeat your order.',
	'commander_erreur_transformation_panier' => 'Error during the creation of the order from the cart. Please repeat your order.',
	'commander_erreur_quantite_0' => 'Please select at least 1 ticket.',

	// T
	'titre_page_configurer_billetteries' => 'Configure ticket offices',
);
