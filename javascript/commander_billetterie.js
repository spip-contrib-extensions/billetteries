
function billetteries_arrondir(nombre, precision=2) {
	return Number.parseFloat(nombre).toFixed(precision);
}

function calculer_total_billets(form) {
	// éviter erreur js si form non éditable
	if (form.find('form').length > 0) {
		var total_quantite = 0;
		var total_prix_ht = 0;
		var total_taxe = 0;
		
		// Calculer chaque ligne et incrémenter le total
		form.find('[data-prix-ht]').each(function() {
			var quantite = Number($(this).val());
			var prix_ht = Number($(this).data('prix-ht'));
			var taxe = Number($(this).data('taxe'));
			var montant_taxe = Number(prix_ht * taxe);
			if (typeof(quantite)!=='undefined' && quantite > 0) {
				total_quantite += quantite;
				total_prix_ht += Number(quantite * prix_ht);
				total_taxe += Number(quantite * montant_taxe);
			}
		});
		
		// Maj affichage
		var label_quantite = form.find('.total-quantity').data('label');
		var label_prix = form.find('.total-price').data('label');
		var label_taxe = form.find('.total-price').data('label-taxe');
		var prix_formater = form.find('.total-price').data('prix-formater');
		var total_prix_ht_formate = prix_formater.replace(/0+[,\.]0+/g, billetteries_arrondir(total_prix_ht));
		var total_taxe_formate = prix_formater.replace(/0+[,\.]0+/g, billetteries_arrondir(total_taxe)) + ' ' + label_taxe;
		var class_notax = (billetteries_arrondir(total_taxe) > 0 ? '' : ' tax-fee_none');
		var texte_total_quantite = '';
		var texte_total_prix_ht = '';
		if (total_quantite > 0) {
			texte_total_quantite = '<span class="label">' + label_quantite + '</span> : ' + '<span class="quantity">' + total_quantite + '</span>';
			texte_total_prix_ht = '<span class="label">' + label_prix + '</span> : <span class="price">' + total_prix_ht_formate + '</span> <span class="tax-fee'+class_notax+'">+ '+total_taxe_formate+'</span>';
		}
		form.find('.total-quantity').html(texte_total_quantite);
		form.find('.total-price').html(texte_total_prix_ht);
	}
}
