<?php
/**
 * Gestion du formulaire de d'édition de billet
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');


/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_billet
 *     Identifiant du billet. 'new' pour un nouveau billet.
 * @param int $id_billets_type
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un billet source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_billet_identifier_dist($id_billet = 'new', $id_billets_type = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_billet)));
}

/**
 * Liste des saisies du formulaire
 *
 * @param int|string $id_billet
 *     Identifiant du billet. 'new' pour un nouveau billet.
 * @param int $id_billets_type
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un billet source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_billet_saisies_dist($id_billet = 'new', $id_billets_type = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$id_billet = intval($id_billet);
	$billet = sql_fetsel('infos, statut', 'spip_billets', "id_billet = $id_billet");
	$statut = (isset($billet['statut']) ? $billet['statut'] : null);
	$infos = (isset($billet['infos']) ? $billet['infos'] : null);
	$infos = ($infos ? unserialize($infos) : null);
	$saisies = array(
		array(
			'saisie' => 'billets_types',
			'options' => array(
				'nom' => 'id_billets_type',
				'obligatoire' => 'oui',
				'label' => _T('billets_type:titre_billets_type'),
			),
		),
		array(
			'saisie' => 'auteurs',
			'options' => array(
				'nom' => 'id_auteur',
				'label' => _T('billet:champ_id_auteur_label'),
				'class' => 'select2',
			),
		),
		array(
			'saisie' => 'auteurs',
			'options' => array(
				'nom' => 'id_auteur_acheteur',
				'label' => _T('billet:champ_id_auteur_acheteur_label'),
				'class' => 'select2',
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'code',
				'label' => _T('billet:champ_code_label'),
				'explication' => _T('billet:champ_code_explication'),
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'email',
				'label' => _T('billet:champ_email_label'),
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'source',
				'label' => _T('billet:champ_source_label'),
				'explication' => _T('billet:champ_source_explication'),
			),
		),
		// Les infos sont sérialisées, on ne peut pas les éditer manuellement
		// On les met en hidden, avec un aperçu en JSON
		array(
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'infos',
			),
		),
		array(
			'saisie' => 'textarea',
			'options' => array(
				'nom' => 'infos_preview',
				'label' => _T('billet:champ_infos_label'),
				'explication' => _T('billet:champ_infos_explication'),
				'readonly' => 'oui',
				'disabled' => 'oui',
				'valeur_forcee' => ($infos ? json_encode($infos, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT) : null),
			),
		),
	);

	// Si c'est un nouveau billet ou qu'il est au panier,
	// certains champs vides seront remplis automatiquement,
	// On ajoute des explications.
	if (
		!$id_billet
		or $statut === 'panier'
	) {
		// Code
		$saisies[3]['options']['explication'] .= '<br />' . _T('billet:champ_vide_explication');
		// Email (auto-rempli uniquement s'il y a un id_auteur)
		$saisies[4]['options']['explication'] =  _T('billet:champ_email_vide_explication');
		// Source (auto-rempli uniquement dans le privé)
		if (test_espace_prive()) {
			$saisies[5]['options']['explication'] .= '<br />' . _T('billet:champ_vide_explication');
		}
	}

	return $saisies;
}

/**
 * Chargement du formulaire d'édition de billet
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_billet
 *     Identifiant du billet. 'new' pour un nouveau billet.
 * @param int $id_billets_type
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un billet source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_billet_charger_dist($id_billet = 'new', $id_billets_type = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('billet', $id_billet, $id_billets_type, $lier_trad, $retour, $config_fonc, $row, $hidden);
	if (!$valeurs['id_billets_type']) {
		$valeurs['id_billets_type'] = $id_billets_type;
	}
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de billet
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_billet
 *     Identifiant du billet. 'new' pour un nouveau billet.
 * @param int $id_billets_type
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un billet source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_billet_verifier_dist($id_billet = 'new', $id_billets_type = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$erreurs = array();

	$erreurs = formulaires_editer_objet_verifier('billet', $id_billet, array('id_billets_type'));

	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de billet
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_billet
 *     Identifiant du billet. 'new' pour un nouveau billet.
 * @param int $id_billets_type
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un billet source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_billet_traiter_dist($id_billet = 'new', $id_billets_type = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$retours = formulaires_editer_objet_traiter('billet', $id_billet, $id_billets_type, $lier_trad, $retour, $config_fonc, $row, $hidden);
	return $retours;
}
