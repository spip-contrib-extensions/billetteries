<?php
/**
 * Gestion du formulaire de d'édition de billetterie
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Saisies d'une billetterie
 *
 * @param int|string $id_billetterie
 *     Identifiant du billetterie. 'new' pour un nouveau billetterie.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier la billetterie créée à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad
 *     Identifiant éventuel d'un billetterie source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billetterie, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_billetterie_saisies_dist($id_billetterie = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {

	// Retrouver le titre de l'objet associé
	if (
		$associer_objet
		and list($objet, $id_objet) = explode('|', $associer_objet)
	) {
		include_spip('inc/filtres');
		$titre_parent = generer_info_entite($id_objet, $objet, 'titre', '*');
	}

	$saisies = array(
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'id_billetterie',
				'type' => 'hidden',
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'titre',
				'label' => _T('billetterie:champ_titre_label'),
				'obligatoire' => 'oui',
				'defaut' => $titre_parent ?? '',
				'class' => 'multilang',
			),
		),
		array(
			'saisie' => 'textarea',
			'options' => array(
				'nom' => 'descriptif',
				'label' => _T('billetterie:champ_descriptif_label'),
				'inserer_barre' => 'forum',
				'rows' => 4,
				'class' => 'multilang',
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'quota',
				'label' => _T('billetterie:champ_quota_label'),
				'explication' => _T('billetterie:champ_quota_explication'),
				'type' => 'number',
				'min' => 0,
			),
			'verifier' => array(
				'type' => 'entier',
				'options' => array(
					'min' => 0,
				),
			),
		),
		array(
			'saisie' => 'date',
			'options' => array(
				'nom' => 'date_debut',
				'label' => _T('billetterie:champ_date_debut_label'),
				'horaire' => 'oui',
			),
			'verifier' => array(
				'type' => 'date',
				'options' => array(
					'normaliser' => 'datetime',
				),
			),
		),
		array(
			'saisie' => 'date',
			'options' => array(
				'nom' => 'date_fin',
				'label' => _T('billetterie:champ_date_fin_label'),
				'horaire' => 'oui',
			),
			'verifier' => array(
				'type' => 'date',
				'options' => array(
					'normaliser' => 'datetime',
				),
			),
		),
		array(
			'saisie' => 'case',
			'options' => array(
				'nom' => 'liste_attente',
				'label_case' => _T('billetterie:champ_liste_attente_label_case'),
				'valeur_oui' => 1,
			),
		),
	);


	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_billetterie
 *     Identifiant du billetterie. 'new' pour un nouveau billetterie.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier la billetterie créée à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad
 *     Identifiant éventuel d'un billetterie source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billetterie, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_billetterie_identifier_dist($id_billetterie = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_billetterie)));
}

/**
 * Chargement du formulaire d'édition de billetterie
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_billetterie
 *     Identifiant du billetterie. 'new' pour un nouveau billetterie.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier la billetterie créée à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad
 *     Identifiant éventuel d'un billetterie source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billetterie, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_billetterie_charger_dist($id_billetterie = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('billetterie', $id_billetterie, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de billetterie
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_billetterie
 *     Identifiant du billetterie. 'new' pour un nouveau billetterie.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier la billetterie créée à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad
 *     Identifiant éventuel d'un billetterie source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billetterie, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_billetterie_verifier_dist($id_billetterie = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$erreurs = array();

	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de billetterie
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_billetterie
 *     Identifiant du billetterie. 'new' pour un nouveau billetterie.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier la billetterie créée à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad
 *     Identifiant éventuel d'un billetterie source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billetterie, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_billetterie_traiter_dist($id_billetterie = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$retours = formulaires_editer_objet_traiter('billetterie', $id_billetterie, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

	// Association éventuelle
	if (
		$associer_objet
		and isset($retours['id_billetterie'])
	) {
		$id_billetterie = $retours['id_billetterie'];
		list($objet, $id_objet) = explode('|', $associer_objet);
		if ($objet and $id_objet and autoriser('modifier', $objet, $id_objet)) {
			include_spip('action/editer_liens');
			objet_associer(array('billetterie' => $id_billetterie), array($objet => $id_objet));
			if (isset($retours['redirect'])) {
				$retours['redirect'] = parametre_url($retours['redirect'], 'id_lien_ajoute', $id_billetterie, '&');
			}
		}
	}

	return $retours;
}
