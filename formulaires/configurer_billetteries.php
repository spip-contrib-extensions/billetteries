<?php
/**
 * Gestion du formulaire de configuration du plugin billetteries
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Saisies d'une billetterie
 *
 * @return array
 */

function formulaires_configurer_billetteries_saisies_dist() {
	include_spip('inc/config');

	$saisies = array(
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'taxe',
				'defaut' => lire_config('billetteries/taxe'),
				'label' => _T('billetteries:cfg_taxe_label'),
				'explication' => _T('billetteries:cfg_taxe_explication'),
				'type' => 'number',
				'step' => '0.001',
			),
			'verifier' => array(
				'type' => 'decimal',
				'options' => array(
					'min' => 0,
					'max' => 1,
				),
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'duree_panier',
				'defaut' => lire_config('billetteries/duree_panier', 30),
				'label' => _T('billetteries:cfg_duree_panier_label'),
				'explication' => _T('billetteries:cfg_duree_panier_explication'),
				'type' => 'number',
				'step' => '1',
			),
			'verifier' => array(
				'type' => 'entier',
				'options' => array(
					'min' => 1,
				),
			),
		),
		array(
			'saisie' => 'choisir_objets',
			'options' => array(
				'nom' => 'objets',
				'label' => _T('billetteries:cfg_objets_label'),
				'explication' => _T('billetteries:cfg_objets_explication'),
				'exclus' => array('spip_billetteries', 'spip_billets_types', 'spip_billets'),
				'defaut' => lire_config('billetteries/objets'),
			),
		),
		array(
			'saisie' => 'case',
			'options' => array(
				'nom' => 'limiter_nb_billetteries_liees',
				'defaut' => lire_config('billetteries/limiter_nb_billetteries_liees'),
				'label_case' => _T('billetteries:cfg_limiter_nb_billetteries_liees_label'),
			),
		),
	);

	// S'il y a notif avancées, on peut configurer à qui on envoie
	if (defined('_DIR_PLUGIN_NOTIFAVANCEES')) {
		$saisies[] = array(
			'saisie' => 'auteurs',
			'options' => array(
				'nom' => 'ids_admins_annulation',
				'label' => _T('billetteries:cfg_ids_admins_annulations_label'),
				'defaut' => lire_config('billetteries/ids_admins_annulation'),
				'multiple' => 'oui',
				'statut' => '0minirezo',
			),
		);
	} else {
		$saisies[] = array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'exp_notifications',
				'texte' => _T('billetteries:cfg_notifications_plugin_explication'),
			),
		);
	}

	return $saisies;
}
