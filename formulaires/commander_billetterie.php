<?php
/**
 * Gestion du formulaire de commande de billets
 * 
 * Il s'agit pour l'instant des billets *d'une seule* billetterie.
 * 2 fonctionnements différents :
 * - Par défaut : permet de commander plusieurs billets d'un coup, dans les quantités qu'on veut.
 * - Avec l'option billet_unitaire : permet de ne commander qu'un seul billet à la fois.
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Retourne les options du formulaires
 *
 * @param int|string $id_billetterie
 * @param string|null $retour
 * @param array $options
 */
function formulaires_commander_billetterie_options_dist($id_billetterie = 0, $retour = '', $options = array()) {
	$options_defaut = [
		// Texte du bouton principal
		'texte_submit' => _T('billetterie:bouton_commander'),
		// Texte des billets gratuits : on garde le défaut de la saisie
		'texte_gratuit' => null,
		// Pour ne permettre de commander qu'un billet à la fois
		'billet_untaire' => false,
		// Si billet unitaire, texte du label (défaut = celui de la saisie → rien)
		'billet_unitaire_label' => null,
		// Si billet unitaire, classes du bouton (défaut = celui de la saisie)
		'billet_unitaire_bouton_class' => null,
		// Si billet unitaire, texte des boutons quand plusieurs types possibles (défaut = celui de la saisie)
		'billet_unitaire_bouton_texte_choix_multiple' => null,
		// Si billet unitaire, texte du bouton quand un seul type possible (défaut = celui de la saisie)
		'billet_unitaire_bouton_texte_choix_unique' => null,
	];
	$options = array_merge($options_defaut, $options);

	return $options;
}

/**
 * Saisies d'une billetterie
 *
 * @param int|string $id_billetterie
 *     Identifiant de la billetterie.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param $options
 *     Tableau d'options éventuelles : voir la fonction pour le détail
 * @return array
 *     Liste des saisies
 */
function formulaires_commander_billetterie_saisies_dist($id_billetterie = 0, $retour = '', $options = array()) {
	if (!function_exists('montant_formater')) {
		include_spip('public/parametrer');
	}

	// Récupérer les options
	$get_options = charger_fonction('options', 'formulaires/commander_billetterie');
	$options = $get_options($id_billetterie, $retour, $options);
	$texte_submit = $options['texte_submit'];
	$texte_gratuit = $options['texte_gratuit'];
	$billet_unitaire = !empty($options['billet_unitaire']);
	$billet_unitaire_bouton_texte_choix_unique = $options['billet_unitaire_bouton_texte_choix_unique'];
	$billet_unitaire_bouton_texte_choix_multiple = $options['billet_unitaire_bouton_texte_choix_multiple'];
	$billet_unitaire_bouton_class = $options['billet_unitaire_bouton_class'];
	$billet_unitaire_label = $options['billet_unitaire_label'];

	// JS pour recalculer le total uniquement si on peut modifier les quantités,
	// donc pas besoin avec l'option billet unitaire.
	// Comme ça ne marche qu'en JS de toute façon, on ajoute en JS uniquement le HTML pour les totaux
	// On l'ajoute depuis le PHP car on a besoin de chaines de langue, donc pas dans le JS statique.
	$js = null;
	if ($billet_unitaire === false) {
		$js = "<script type=\"text/javascript\">
;jQuery(function($) {
	$(function() {
		$('.formulaire_commander_billetterie').each(function(){
			var form = $(this);
			var boutons = form.find('.boutons');
			
			// Si pas déjà
			if (boutons.find('[data-total-quantite]').length == 0) {
				var total_quantite = $('<span/>')
					.addClass('total-quantity')
					.data('total-quantite', 0)
					.data('label', '" . _T('billetterie:champ_quantite_label') . "');
				var total_prix = $('<span/>')
					.addClass('total-price')
					.data('total-prix-ht', 0)
					.data('label', '" . _T('billetterie:champ_total_label') . "')
					.data('label-taxe', '" . _T('billetterie:champ_taxe_label') . "')
					.data('prix-formater', '" . preg_replace('/[\n\r]+/', '', addslashes(montant_formater('', array('markup' => false)))) . "');
				
				// On ajoute
				boutons.prepend(total_prix).prepend(total_quantite);
			}
			
			// On lance le calcul
			calculer_total_billets(form);
			
			// Et à chaque changement
			form.find('[data-prix-ht]').change(function() {
				calculer_total_billets(form);
			});
		});
	});
});
</script>";
	}
	// Du CSS pour cacher le bouton submit principal en cas de billet unitaire
	// Il manque une option générale pour cela dans Saisies
	$css = null;
	if ($billet_unitaire === true) {
		$css = "\n<style>.formulaire_spip.formulaire_commander_billetterie .boutons { display: none; }</style>";
	}

	$saisies = array(
		'options' => array(
			'texte_submit' => $texte_submit,
			'inserer_fin' => $js . $css,
		),
	);
	
	if (
		$id_billetterie = intval($id_billetterie)
		and $billetterie = sql_fetsel('*', 'spip_billetteries', 'id_billetterie = '.$id_billetterie)
	) {

		// Compter le nombre de types de billets dispos
		$nb_billets_types_disponibles = count(billetteries_ids_billets_types_disponibles($id_billetterie));

		// Infos d'ouverture de cette billetterie s'il y a plusieurs types de billets.
		// Sinon pas la peine, déjà affiché avec chaque type de billet.
		if (
			$nb_billets_types_disponibles > 1
			and $billetterie_ouverture = affdate_debut_fin_billetterie($billetterie['date_debut'], $billetterie['date_fin'])
		) {
			$saisies[] = array(
				'saisie' => 'explication',
				'options' => array(
					'nom' => 'billetterie_ouverture',
					'texte' => _T('billetterie:titre_billetterie_ouverte').' '.$billetterie_ouverture,
				),
			);
		}

		// Info de quota général s'il y a plusieurs types de billets.
		// Sinon pas la peine, déjà affiché avec chaque type de billet.
		if (
			$nb_billets_types_disponibles > 1
			and $billetterie_dispo = billetteries_compter_billets_disponibles($id_billetterie)
			and is_numeric($billetterie_dispo)
		) {
			$saisies[] = array(
				'saisie' => 'explication',
				'options' => array(
					'nom' => 'billetterie_dispo',
					'texte' => _T('billetterie:info_nb_dispo', array('nb' => $billetterie_dispo)),
				),
			);
		}

		// Tous les types de billets de cette billetterie
		if ($billets_types = sql_allfetsel('*', 'spip_billets_types', 'id_billetterie='.intval($id_billetterie), '', 'rang ASC')) {
			foreach($billets_types as $billets_type) {
				$id_billets_type = $billets_type['id_billets_type'];
				$min = ($billets_type['selection_min'] ? intval($billets_type['selection_min']) : null);
				$max = ($billets_type['selection_max'] ? intval($billets_type['selection_max']) : null);

				// On teste la disponibilité
				$billets_type_ouvert = billetteries_tester_dates_ouverture($id_billets_type, 'billets_type');
				$billets_type_dispo = billetteries_compter_billets_disponibles($id_billets_type, 'billets_type');
				$est_disponible = ($billets_type_ouvert && $billets_type_dispo);

				// min : doît être au moins 0, et pas inférieur
				$valeurs_min = ($min ? array($min) : array());
				$valeurs_min[] = 0;
				$min = max($valeurs_min);

				// max : ne peut être supérieur au nombre de billets dispos (qui tient compte du quota)
				// Attention le nombre de billets dispos peut être 'true' si aucun quota
				$valeurs_max = ($max ? array($max) : array());
				if (is_numeric($billets_type_dispo)) {
					$valeurs_max[] = intval($billets_type_dispo);
				}
				$max = (count($valeurs_max) ? min($valeurs_max) : null);

				// Si on prend un billet unitaire, min et max = 1 quelle que soit la config
				if ($billet_unitaire === false) {
					$min = $max = 1;
				}

				// Si le max est plus petit que le min, on rend indisponible
				// ex : si pour un billet faut en prendre au moins 2, mais qu'il n'en reste plus qu'un de dispo
				if ($max and ($max < $min)) {
					$est_disponible = false;
				}

				// Vérification
				$options_verifier = array(
					'min' => $min,
				);
				if ($max) {
					$options_verifier['max'] = $max;
				}

				// On ajoute la saisie
				$saisies[] = array(
					'saisie' => 'billets_type_quantite',
					'options' => array(
						'nom' => "quantites[$id_billets_type]",
						'id_billets_type' => $id_billets_type,
						'label' => _T('billetterie:champ_quantite_label'),
						'disponible' => $est_disponible,
						'nb_dispo' => ($est_disponible ? $billets_type_dispo : false),
						'min' => $min,
						'max' => $max,
						'texte_gratuit' => $texte_gratuit,
						'billet_unitaire' => $billet_unitaire,
						'billet_unitaire_bouton_texte' => (($nb_billets_types_disponibles > 1) ? $billet_unitaire_bouton_texte_choix_multiple : $billet_unitaire_bouton_texte_choix_unique),
						'billet_unitaire_bouton_class' => $billet_unitaire_bouton_class,
						'billet_unitaire_label' => $billet_unitaire_label,
					),
					'verifier' => array(
						'type' => 'entier',
						'options' => $options_verifier,
					),
				);
			}
		}
	}
	
	return $saisies;
}

/**
 * Chargement du formulaire de commande de billets
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @param int|string $id_billetterie
 *     Identifiant du billetterie.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param $options
 *     Tableau d'options éventuelles
 * @return array
 *     Environnement du formulaire
 */
function formulaires_commander_billetterie_charger_dist($id_billetterie = 0, $retour = '', $options = array()) {
	// Nettoyer tout ce qui est trop vieux
	if (!function_exists('billetteries_nettoyer_paniers')) {
		include_spip('public/parametrer');
	}
	billetteries_nettoyer_paniers();
	
	// Vérifier les dates d'ouverture et le quota
	$editable = billetteries_tester_disponibilite($id_billetterie);
	
	$valeurs = array(
		'editable'       => $editable,
		'id_billetterie' => $id_billetterie,
		'quantites'      => _request('quantites'),
	);

	if (!$editable) {
		$valeurs['message_erreur'] = _T('billetterie:message_billetterie_indisponible');
	}

	return $valeurs;
}

/**
 * Vérifications du formulaire de commande de billets
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @param int|string $id_billetterie
 *     Identifiant de la billetterie.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param $options
 *     Tableau d'options éventuelles
 * @return array
 *     Tableau des erreurs
 */
function formulaires_commander_billetterie_verifier_dist($id_billetterie = 0, $retour = '', $options = array()) {
	$erreurs = array();
	
	$quantites = array_filter(_request('quantites'));

	// Ne pas commander du vide !
	if (count($quantites) === 0) {
		$erreurs['message_erreur'] = _T('billetteries:commander_erreur_quantite_0');

	// Avant d'aller enregistrer, on reteste si le choix est encore disponible
	} elseif (
		$id_billetterie = intval($id_billetterie)
	) {
		$quantite_totale = 0;
		foreach ($quantites as $id_billets_type => $quantite) {
			// Total
			$quantite_totale = $quantite_totale + $quantite;
			
			// On vérifie qu'il reste assez de places pour ce type de billets
			if (
				!$billets_type_dispo = billetteries_compter_billets_disponibles($id_billets_type, 'billets_type')
				or (is_numeric($billets_type_dispo) and $quantite > $billets_type_dispo)
			) {
				$erreurs["quantites[$id_billets_type]"] = _T('billetteries:commander_erreur_billets_type_quantite', array('nb' => $billets_type_dispo));
			}
		}
		
		// On teste le quota total aussi
		if (
			!$billetterie_dispo = billetteries_compter_billets_disponibles($id_billetterie)
			or (is_numeric($billetterie_dispo) and $quantite_totale > $billetterie_dispo)
		) {
			$erreurs['message_erreur'] = _T('billetteries:commander_erreur_billetterie_quantite', array('nb' => $billetterie_dispo));
		}
	}

	return $erreurs;
}

/**
 * Traitement du formulaire de commande de billets
 *
 * - Poser un cookie
 * - Supprimer d'éventuels anciens billets au panier
 * - Créer des billets au panier avec le cookie
 *
 * @param int|string $id_billetterie
 *     Identifiant du billetterie. 'new' pour un nouveau billetterie.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param $options
 *     Tableau d'options éventuelles
 * @return array
 *     Retours des traitements
 */
function formulaires_commander_billetterie_traiter_dist($id_billetterie = 0, $retour = '', $options = array()) {
	include_spip('inc/acces');
	include_spip('inc/cookie');
	include_spip('action/editer_objet');
	include_spip('inc/session');
	include_spip('billetteries_fonctions');
	
	$retours = array();
	$nom_cookie = $GLOBALS['cookie_prefix'] . '_panier_billetteries';
	// Si on le connait déjà, tant qu'à faire…
	$id_auteur_acheteur = session_get('id_auteur') ? intval(session_get('id_auteur')) : 0;
	$ok = true;
	$nb_erreurs = 0;
	$nb_total = 0;
	
	// On supprime tous les trucs en panier et commande
	billetteries_supprimer_panier_visiteur();
	
	// Dans tous les cas on refait un nouveau cookie à zéro puisque le panier est refait
	$cookie = creer_uniqid();
	spip_setcookie(
		$nom_cookie,
		$_COOKIE[$nom_cookie] = $cookie,
		time() + 60 * lire_config('billetteries/duree_panier', 30)
	);
	
	// C'est parti pour l'enregistrement des billets
	$quantites = (is_array(_request('quantites')) ? _request('quantites') : array());
	$quantites = array_map('intval', $quantites);
	$quantites = array_filter($quantites);
	foreach ($quantites as $id_billets_type => $quantite) {
		for ($i=1; $i<=$quantite; $i++) {
			$nb_total++;
			$id_billet = objet_inserer(
				'billet',
				$id_billets_type,
				array(
					'id_billets_type'    => $id_billets_type,
					'cookie'             => $cookie,
					'id_auteur_acheteur' => $id_auteur_acheteur,
					'source'             => 'commander_billetterie#' . $id_billetterie,
				)
			);
			// Erreur dès qu'il y a un billet non enregistré
			if (!$id_billet) {
				$ok = false;
				$nb_erreurs++;
			}
		}
	}
	
	// Erreur
	if (!$ok) {
		$infos_erreur = array(
			'id_auteur_acheteur' => $id_auteur_acheteur,
			'cookie'             => $cookie,
			'quantites'          => $quantites,
		);
		spip_log("[commander_billetterie] Billet(s) non enregistré(s) : $nb_erreurs / $nb_total. Infos : " . json_encode($infos_erreur), 'billetteries' . _LOG_ERREUR);
		$retours['message_erreur'] = _T('billetteries:commander_erreur_enregistrement');
	}
	
	// Redirection
	if ($ok and $retour) {
		$retours['redirect'] = $retour;
	}

	return $retours;
}