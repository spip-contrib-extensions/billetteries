<?php
/**
 * Gestion du formulaire de d'édition de billets_type
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Saisies des types de billets
 *
 * @param int|string $id_billets_type
 *     Identifiant du billets_type. 'new' pour un nouveau billets_type.
 * @param int $id_billetterie
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un billets_type source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billets_type, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_billets_type_saisies_dist($id_billets_type = 'new', $id_billetterie = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {

	include_spip('inc/config');

	$saisies = array(
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'id_billets_type',
				'type' => 'hidden',
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'titre',
				'label' => _T('billets_type:champ_titre_label'),
				'obligatoire' => 'oui',
				'class' => 'multilang',
			),
		),
		array(
			'saisie' => 'billetteries',
			'options' => array(
				'nom' => 'id_billetterie',
				'label' => _T('billets_type:champ_id_billetterie_label'),
				'obligatoire' => 'oui',
				'class' => 'chosen',
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'prix_ht',
				'label' => _T('billets_type:champ_prix_ht_label'),
				'obligatoire' => 'oui',
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'taxe',
				'defaut' => lire_config('billetteries/taxe'),
				'label' => _T('billets_type:champ_taxe_label'),
				'explication' => _T('billets_type:champ_taxe_explication'),
				'obligatoire' => 'oui',
				'type' => 'number',
				'step' => '0.001',
			),
			'verifier' => array(
				'type' => 'decimal',
				'options' => array(
					'min' => 0,
					'max' => 1,
				),
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'quota',
				'label' => _T('billets_type:champ_quota_label'),
				'explication' => _T('billets_type:champ_quota_explication'),
				'type' => 'number',
				'min' => 0,
			),
			'verifier' => array(
				'type' => 'entier',
				'options' => array(
					'min' => 0,
				),
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'selection_min',
				'label' => _T('billets_type:champ_selection_min_label'),
				'explication' => _T('billets_type:champ_selection_min_explication'),
				'type' => 'number',
				'min' => 0,
			),
			'verifier' => array(
				'type' => 'entier',
				'options' => array(
					'min' => 0,
				)
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'selection_max',
				'label' => _T('billets_type:champ_selection_max_label'),
				'explication' => _T('billets_type:champ_selection_max_explication'),
				'type' => 'number',
				'min' => 0,
			),
			'verifier' => array(
				'type' => 'entier',
				'options' => array(
					'min' => 0,
				)
			),
		),
		array(
			'saisie' => 'date',
			'options' => array(
				'nom' => 'date_debut',
				'label' => _T('billets_type:champ_date_debut_label'),
				'horaire' => 'oui',
			),
			'verifier' => array(
				'type' => 'date',
				'options' => array(
					'normaliser' => 'datetime',
				),
			),
		),
		array(
			'saisie' => 'date',
			'options' => array(
				'nom' => 'date_fin',
				'label' => _T('billets_type:champ_date_fin_label'),
				'horaire' => 'oui',
			),
			'verifier' => array(
				'type' => 'date',
				'options' => array(
					'normaliser' => 'datetime',
				),
			),
		),
		array(
			'saisie' => 'textarea',
			'options' => array(
				'nom' => 'descriptif',
				'label' => _T('billets_type:champ_descriptif_label'),
				'inserer_barre' => 'forum',
				'rows' => 4,
				'class' => 'multilang',
			),
		),
	);

	// Ajuster les saisies en fonction du paramétrage de la billetterie parente
	$billetterie = sql_fetsel('quota, date_debut, date_fin', 'spip_billetteries', 'id_billetterie='.intval($id_billetterie));
	// Quota
	if (intval($quota = $billetterie['quota'])) {
		$saisies[5]['options']['explication'] .= '. ' . _T('billets_type:champ_quota_max_explication', array('nb' => $quota));;
		$saisies[5]['options']['max'] = $quota;
		$saisies[5]['verifier']['options']['max'] = $quota;
	}
	// Dates
	if ($date_debut = filtrer_date_vide($billetterie['date_debut'])) {
		$explication = $saisies[8]['options']['explication'] ?? '';
		$saisies[8]['options']['explication'] = $explication . _T('billets_type:champ_date_min_explication', ['date' => affdate($date_debut)]);
	}
	if ($date_fin = filtrer_date_vide($billetterie['date_fin'])) {
		$explication = $saisies[9]['options']['explication'] ?? '';
		$saisies[9]['options']['explication'] = $explication . _T('billets_type:champ_date_max_explication', array('date' => affdate($date_fin)));
	}

	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_billets_type
 *     Identifiant du billets_type. 'new' pour un nouveau billets_type.
 * @param int $id_billetterie
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un billets_type source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billets_type, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_billets_type_identifier_dist($id_billets_type = 'new', $id_billetterie = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_billets_type)));
}

/**
 * Chargement du formulaire d'édition de billets_type
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_billets_type
 *     Identifiant du billets_type. 'new' pour un nouveau billets_type.
 * @param int $id_billetterie
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un billets_type source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billets_type, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_billets_type_charger_dist($id_billets_type = 'new', $id_billetterie = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('billets_type', $id_billets_type, $id_billetterie, $lier_trad, $retour, $config_fonc, $row, $hidden);
	if (!$valeurs['id_billetterie']) {
		$valeurs['id_billetterie'] = $id_billetterie;
	}
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de billets_type
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_billets_type
 *     Identifiant du billets_type. 'new' pour un nouveau billets_type.
 * @param int $id_billetterie
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un billets_type source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billets_type, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_billets_type_verifier_dist($id_billets_type = 'new', $id_billetterie = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$erreurs = formulaires_editer_objet_verifier('billets_type', $id_billets_type, array('titre', 'id_billetterie'));

	$id_billetterie = _request('id_billetterie');
	$billetterie = sql_fetsel('*', 'spip_billetteries', 'id_billetterie='.intval($id_billetterie));

	// le quota ne peut excéder celui de la billetterie
	$quota_billetterie = intval($billetterie['quota']);
	$quota = intval(_request('quota'));
	if (
		$quota
		and $quota_billetterie
		and ($quota > $quota_billetterie)
	) {
		$erreurs['quota'] = _T('billets_type:erreur_quota_billetterie', array('quota'=>$quota_billetterie));
	}

	// la sélection max ne peut éxceder le quota
	$selection_max = intval(_request('selection_max'));
	if (
		$selection_max
		and $quota
		and $selection_max > $quota
	) {
		$erreurs['selection_max'] = _T('billets_type:erreur_selection_max_quota');
	}

	// selection max doit être inférieure au quota.

	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de billets_type
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_billets_type
 *     Identifiant du billets_type. 'new' pour un nouveau billets_type.
 * @param int $id_billetterie
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un billets_type source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du billets_type, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_billets_type_traiter_dist($id_billets_type = 'new', $id_billetterie = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$retours = formulaires_editer_objet_traiter('billets_type', $id_billets_type, $id_billetterie, $lier_trad, $retour, $config_fonc, $row, $hidden);
	return $retours;
}
