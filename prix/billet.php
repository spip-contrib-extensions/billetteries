<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function prix_billet_ht_dist($id_billet, $ligne) {
	$prix_ht = 0;
	
	if (
		$id_billets_type = intval($ligne['id_billets_type'])
		and $fonction_prix_ht = charger_fonction('ht', 'inc/prix', true)
	) {
		$prix_ht = $fonction_prix_ht('billets_type', $id_billets_type);
	}
	
	return $prix_ht;
}
