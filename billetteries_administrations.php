<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Billetteries
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'installation et de mise à jour du plugin Billetteries.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function billetteries_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	$maj['create'] = array(array('maj_tables', array('spip_billetteries', 'spip_billetteries_liens', 'spip_billets_types', 'spip_billets')));

	// 1.0.1 : ajout de id_billetterie sur spip_billets
	$maj['1.0.1'] = array(
		array('maj_tables', array('spip_billets')),
		array('billetteries_maj_101'),
	);

	// 1.0.2 : plus de clés
	$maj['1.0.2'] = array(
		array('maj_tables', array('spip_billets')),
	);

	// 1.0.3 : renommage prix → prix_ht
	$maj['1.0.3'] = array(
		array('sql_alter', 'TABLE spip_billets_types CHANGE prix prix_ht decimal(20,6) NOT NULL DEFAULT 0'),
	);

	// 1.0.4 : graver les infos des types dans les billets
	$maj['1.0.4'] = array(
		array('billetteries_maj_104'),
	);

	// 1.0.5 : Changer le type du champ code en varchar au lieu de text
	$maj['1.0.5'] = array(
		array('sql_alter', 'TABLE spip_billets CHANGE code code varchar(255) NOT NULL'),
	);

	// 1.1.0 : Ajout du champ source sur les billets
	$maj['1.1.0'] = array(
		array('maj_tables', array('spip_billets')),
	);

	// 1.2.0 : Ajout du champ descriptif sur les billetteries
	$maj['1.2.0'] = array(
		array('maj_tables', array('spip_billetteries')),
	);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin Billetteries.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function billetteries_vider_tables($nom_meta_base_version) {

	sql_drop_table('spip_billetteries');
	sql_drop_table('spip_billetteries_liens');
	sql_drop_table('spip_billets_types');
	sql_drop_table('spip_billets');

	# Nettoyer les liens courants (le génie optimiser_base_disparus se chargera de nettoyer toutes les tables de liens)
	sql_delete('spip_documents_liens', sql_in('objet', array('billetterie', 'billets_type', 'billet')));
	sql_delete('spip_mots_liens', sql_in('objet', array('billetterie', 'billets_type', 'billet')));
	sql_delete('spip_auteurs_liens', sql_in('objet', array('billetterie', 'billets_type', 'billet')));
	# Nettoyer les versionnages et forums
	sql_delete('spip_versions', sql_in('objet', array('billetterie', 'billets_type', 'billet')));
	sql_delete('spip_versions_fragments', sql_in('objet', array('billetterie', 'billets_type', 'billet')));
	sql_delete('spip_forum', sql_in('objet', array('billetterie', 'billets_type', 'billet')));

	effacer_meta($nom_meta_base_version);
}


/**
 * Maj 1.0.1 : remplir les `id_billetterie` des billets
 *
 * @return void
 */
function billetteries_maj_101() {
	include_spip('base/abstract_sql');
	if ($billets = sql_allfetsel('id_billet,id_billets_type', 'spip_billets')) {
		foreach ($billets as $billet) {
			if ($id_billetterie = sql_getfetsel(
				'id_billetterie',
				'spip_billets_types',
				'id_billets_type = ' . intval($billet['id_billets_type'])
			)) {
				$set = array('id_billetterie' => $id_billetterie);
				$where = 'id_billet=' . intval($billet['id_billet']);
				sql_updateq('spip_billets', $set, $where);
			}
		}
	}
}

/**
 * Maj 1.0.4 : graver les infos des types dans les billets
 *
 * @return void
 */
function billetteries_maj_104() {
	include_spip('base/abstract_sql');
	include_spip('action/editer_objet');
	if ($billets = sql_allfetsel('id_billet,id_billets_type,infos', 'spip_billets')) {
		foreach ($billets as $billet) {
			$id_billet = $billet['id_billet'];
			if ($infos_billets_type = sql_fetsel('titre AS titre_billets_type', 'spip_billets_types', 'id_billets_type='.intval($billet['id_billets_type']))) {
				// Récupérer le prix_ht + taxe
				if (
					$fonction_prix_ht = charger_fonction('ht', 'inc/prix', true)
					and $fonction_taxes = charger_fonction('taxes', 'inc/', true)
				) {
					$infos_billets_type['prix_ht'] = $fonction_prix_ht('billet', $id_billet);
					$taxes = $fonction_taxes('billet', $id_billet);
					$taxes_montant = array_sum(array_column($taxes, 'montant'));
					$infos_billets_type['taxes'] = $taxes_montant;
				}
				$infos = is_array(unserialize($billet['infos'])) ? unserialize($billet['infos']) : array();
				$infos_plus = array_merge(
					$infos,
					$infos_billets_type
				);
				$set = array('infos' => serialize($infos_plus));
				objet_modifier('billet', $billet['id_billet'], $set);
			}
		}
	}
}
