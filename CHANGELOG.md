# Changelog

## 1.4.4 - 2023-10-19

- #24 Nouvelle option `billet_unitaire` au formulaire commande_billetterie, permettant de ne commander qu'un seul billet à la fois.

## 1.4.3 - 2023-09-14

### Fixed

- #20 Le cookie est renommé en `xxx_panier_billetteries`
## 1.4.2 - 2023-07-26

### Fixed

- #21 Fix autocompletion uniquement quand billet au panier

### Added

- #21 Possibilité de désactiver ponctuellement l'autocomplétion des billets au panier avec `billetteries_desactiver_auto_completion` à true dans le request

## 1.4.1

### Added

- Compat SPIP 4.2

## 1.4.0 - 2023-06-28

### Fixed

* #21 Fix sur l'autocomplétion à posteriori des billets
* #21 Ergo édition d'un billet : explications pour l'autocomplétion
* #21 Ergo prévisu d'un billet : plus compact, labels manquants

### Added

* Ajout d'un changelog
* Pipeline `billet_completer_champs` permettant d'ajouter des choses à l'autocomplétion à posteriori des billets