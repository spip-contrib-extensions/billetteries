<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function taxes_billet_dist($id_billet, $prix_ht, $options) {
	$taxes = array();
	
	if (
		$id_billet = intval($id_billet)
		and $id_billets_type = intval(sql_getfetsel('id_billets_type', 'spip_billets', 'id_billet = '.$id_billet))
		and $fonction_taxes = charger_fonction('taxes', 'inc/', true)
	) {
		$taxes = $fonction_taxes('billets_type', $id_billets_type, $options);

		// On refait le montant des taxes car il faut prendre le ht du billet, pas du type de billet.
		foreach ($taxes as $k => $taxe) {
			$taux = $taxe['taux'];
			$taxes[$k]['montant'] = $prix_ht * $taux;
		}
	}

	return $taxes;
}
