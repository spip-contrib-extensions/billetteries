<?php
/**
 * Utilisations de pipelines par Billetteries
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Utilisation du pipeline affiche milieu
 * - Ajoute le formulaire d'édition des billetteries sur les objets qui le peuvent
 *
 * @pipeline affiche_milieu
 *
 * @param array $flux
 *     Données du pipeline
 * @return array
 *     Données du pipeline
 */
function billetteries_affiche_milieu($flux) {
	include_spip('inc/config');

	// Si on est sur une page ou il faut inserer les billetteries
	if (
		$en_cours = trouver_objet_exec($flux['args']['exec'])
		and $en_cours['edition'] !== true // page visu
		and in_array($en_cours['table_objet_sql'], lire_config('billetteries/objets', array()))
		and $objet = $en_cours['type']
		and $cle_objet = $en_cours['id_table_objet']
		and isset($flux['args'][$cle_objet])
		and $id_objet = intval($flux['args'][$cle_objet])
	) {
		// On génère l'interface d'édition
		$texte = recuperer_fond(
			'prive/objets/editer/liens',
			array(
				'table_source' => 'billetteries',
				'objet' => $objet,
				'id_objet' => $id_objet,
			)
		);

		// On l'insère dans la page
		if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}

/**
 * Ajouter les objets sur les vues des parents directs
 *
 * - billetterie → types de billets
 * - type de billets → billets
 * - billet → commande
 * - auteur/contact → billets
 *
 * @pipeline affiche_enfants
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function billetteries_affiche_enfants($flux) {
	if ($e = trouver_objet_exec($flux['args']['exec']) and $e['edition'] == false) {
		$id_objet = $flux['args']['id_objet'];

		// Obligé de définir manuellement le contexte utile aux listes de billets
		$contexte_liste_billets = array(
			'tri_liste_billets' => _request('tri_liste_billets'),
			'id_auteur' => _request('id_auteur'),
			'id_auteur_acheteur' => _request('id_auteur_acheteur'),
		);

		// Sous une billetterie, les types et les billets
		if ($e['type'] == 'billetterie') {
			// Liste des types de billets
			$flux['data'] .= recuperer_fond(
				'prive/objets/liste/billets_types',
				array(
					'titre' => _T('billets_type:titre_billets_types'),
					'id_billetterie' => $id_objet
				)
			);

			// Icône créer type de billet
			if (autoriser('creerbilletstypedans', 'billetteries', $id_objet)) {
				include_spip('inc/presentation');
				$flux['data'] .= icone_verticale(
					_T('billets_type:icone_creer_billets_type'),
					generer_url_ecrire('billets_type_edit', "id_billetterie=$id_objet"),
					'billets_type-24.png',
					'new',
					'right'
				) . "<br class='nettoyeur' />";
			}

			// Liste des billets
			$flux['data'] .= recuperer_fond(
				'prive/objets/liste/billets',
				array_merge(
					$contexte_liste_billets,
					array(
						'id_billetterie' => $id_objet,
					)
				)
			);
		}

		// Dans un type de billet, tous les billets de ce type
		elseif ($e['type'] == 'billets_type') {
			$flux['data'] .= recuperer_fond(
				'prive/objets/liste/billets',
				array_merge(
					$contexte_liste_billets,
					array(
						'id_billets_type' => $id_objet
					)
				)
			);

			if (autoriser('creerbilletdans', 'billets_types', $id_objet)) {
				include_spip('inc/presentation');
				$flux['data'] .= icone_verticale(
					_T('billet:icone_creer_billet'),
					generer_url_ecrire('billet_edit', "id_billets_type=$id_objet"),
					'billet-24.png',
					'new',
					'right'
				) . "<br class='nettoyeur' />";
			}
		}

		// Dans un billet, la commande liée (pas forcément le meilleur endroit ?)
		elseif ($e['type'] == 'billet' and defined('_DIR_PLUGIN_COMMANDES')) {
			include_spip('action/editer_liens');
			$commandes = objet_trouver_liens(array('commande' => '*'), array('billet' => $id_objet));
			if ($ids_commandes = array_column($commandes, 'id_commande')) {
				$flux['data'] .= recuperer_fond(
					'prive/objets/liste/commandes',
					array(
						'id_commande' => $ids_commandes,
					)
				);
			}
		}

		// Dans un auteur ou un contact, les billets
		elseif (
			(
				$e['type'] == 'auteur'
				and $id_auteur = $id_objet
			)
			or (
				$e['type'] == 'contact'
				and $id_auteur = sql_getfetsel('id_auteur', 'spip_contacts', 'id_contact='.intval($id_objet))
			)
		) {
			$flux['data'] .= recuperer_fond(
				'prive/objets/liste/billets',
				array_merge(
					$contexte_liste_billets,
					array(
						'id_auteur' => $id_auteur,
					)
				)
			);
		}
	}
	return $flux;
}

/**
 * Afficher le nombre d'éléments dans les parents
 *
 * @pipeline boite_infos
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function billetteries_boite_infos($flux) {
	if (isset($flux['args']['type']) and isset($flux['args']['id']) and $id = intval($flux['args']['id'])) {
		$texte = '';
		if ($flux['args']['type'] == 'billetterie' and $nb = sql_countsel('spip_billets_types', array('id_billetterie=' . $id))) {
			$texte .= '<div>' . singulier_ou_pluriel($nb, 'billets_type:info_1_billets_type', 'billets_type:info_nb_billets_types') . "</div>\n";
		}
		if ($flux['args']['type'] == 'billets_type' and $nb = sql_countsel('spip_billets', array("statut='publie'", 'id_billets_type=' . $id))) {
			$texte .= '<div>' . singulier_ou_pluriel($nb, 'billet:info_1_billet', 'billet:info_nb_billets') . "</div>\n";
		}
		if ($texte and $p = strpos($flux['data'], '<!--nb_elements-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		}
	}
	return $flux;
}


/**
 * Compter les enfants d'un objet
 *
 * @pipeline objets_compte_enfants
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function billetteries_objet_compte_enfants($flux) {
	if ($flux['args']['objet'] == 'billetterie' and $id_billetterie = intval($flux['args']['id_objet'])) {
		$flux['data']['billets_types'] = sql_countsel('billets_types', 'id_billetterie= ' . intval($id_billetterie));
	}
	if ($flux['args']['objet'] == 'billets_type' and $id_billets_type = intval($flux['args']['id_objet'])) {
		// juste les publiés ?
		if (array_key_exists('statut', $flux['args']) and ($flux['args']['statut'] == 'publie')) {
			$flux['data']['billets'] = sql_countsel('spip_billets', 'id_billets_type= ' . intval($id_billets_type) . " AND (statut = 'publie')");
		} else {
			$flux['data']['billets'] = sql_countsel('spip_billets', 'id_billets_type= ' . intval($id_billets_type) . " AND (statut <> 'poubelle')");
		}
	}

	return $flux;
}

/**
 * Ajouter les billets sur les commandes
 *
 * @pipeline recuperer_fond
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function billetteries_recuperer_fond($flux) {
	if (
		$flux['args']['fond'] == 'prive/objets/contenu/commande'
		and $id_commande = $flux['args']['contexte']['id']
		and $liens = objet_trouver_liens(array('commande'=>$id_commande), array('billet'=>'*'))
		and is_array($liens)
	) {
		$ids_billets = array_column($liens, 'id_objet');

		$flux['data']['texte'] .= recuperer_fond(
			'prive/objets/liste/billets',
			array(
				'id_billet' => $ids_billets,
			)
		);
	}

	return $flux;
}



/**
 * Optimiser la base de données
 *
 * Supprime les liens orphelins de l'objet vers quelqu'un et de quelqu'un vers l'objet.
 * Supprime les objets à la poubelle.
 * Supprime les objets à la poubelle.
 * Supprime les billets et commandes trop vieilles.
 *
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function billetteries_optimiser_base_disparus($flux) {
	include_spip('action/editer_liens');
	include_spip('inc/config');

	$flux['data'] += objet_optimiser_liens(array('billetterie'=>'*'), '*');
	$mydate = sql_quote(trim($flux['args']['date'], "'"));
	sql_delete('spip_billetteries', "statut='poubelle' AND maj < $mydate");

	sql_delete('spip_billets', "statut='poubelle' AND maj < $mydate");

	// Testons la date des paniers
	if ($duree_panier = intval(lire_config('billetteries/duree_panier', 30))) {
		$date_maxi = date('Y-m-d H:i:s', time() - 60 * $duree_panier);

		// On récupère les billets panier trop vieux
		if ($nb_billets = sql_delete(
			'spip_billets',
			array(
				'statut = "panier"',
				'date < '.sql_quote($date_maxi),
			)
		)) {
			$flux['data'] += $nb_billets;
		}

		if (defined('_DIR_PLUGIN_COMMANDES')) {
			// S'il y a bien des commandes à supprimer
			if ($commandes = sql_allfetsel(
				'id_commande',
				'spip_commandes',
				array(
					'statut = "encours"',
					'date < '.sql_quote($date_maxi)
				)
			)) {
				$commandes = array_map('reset', $commandes);
				include_spip('inc/commandes');
				commandes_supprimer($commandes);
				$flux['data'] += count($commandes);
			}
		}
	}

	return $flux;
}

/**
 * Affiche les utilisations d'une billetterie sur sa page + ur les types de billets
 *
 * @param array $flux
 *    Données du pipeline
 * @return array
 */
function billetteries_afficher_config_objet($flux) {
	// Contenus liés aux billetteries
	if (
		(
			$flux['args']['type'] == 'billetterie'
			and $id_billetterie = $flux['args']['id']
		)
		or (
			$flux['args']['type'] == 'billets_type'
			and $id_billetterie = sql_getfetsel('id_billetterie', 'spip_billets_types', 'id_billets_type='.intval($flux['args']['id']))
		)
	) {
		$flux['data'] .= recuperer_fond('prive/squelettes/inclure/billetterie_contenus_lies', array('id_billetterie' => $id_billetterie));
	}
	// Export sur les types de billets
	if (
		$flux['args']['type'] == 'billets_type'
		and isset($flux['args']['id'])
		and $id_billets_type = $flux['args']['id']
	) {
		$exporter = recuperer_fond('prive/squelettes/inclure/billets_exporter', array('id_billets_type'=>$id_billets_type));
		$flux['data'] .= $exporter;
	}

	return $flux;
}

function billetteries_saisies_autonomes($saisies) {
	$saisies[] = 'billets_type_quantite';

	return $saisies;
}

function billetteries_jquery_plugins($scripts) {
	$scripts[] = 'javascript/commander_billetterie.js';

	return $scripts;
}

/**
 * Compléter les traitements des formulaires
 *
 * - Créer la commande finale à la fin des inscriptions ou modifs de profils
 * - Renseigner le `id_auteur_acheteur` des billets du panier (qui sont anonymes avant connexion ou inscription)
 *
 * @pipeline formulaire_traiter
 * @param array $flux
 * 		Flux du pipeline contenant toutes les saisies des formulaires
 * @return array
 * 		Retourne le flux possiblement modifié
 **/
function billetteries_formulaire_traiter($flux) {
	// Seulement si ya bien le plugin Commandes
	if (defined('_DIR_PLUGIN_COMMANDES')) {
		$formulaires = pipeline(
			'billetteries_generer_commande_apres_formulaires',
			array('editer_auteur', 'inscription', 'profil')
		);

		if (
			is_array($formulaires)
			and in_array($flux['args']['form'], $formulaires)
			and $id_auteur = $flux['data']['id_auteur']
			// Si Profils, on ne crée de commande que si on édite son propre id_auteur, pas un autre
			and (!isset($flux['args']['args'][3]['forcer_admin']) or !$flux['args']['args'][3]['forcer_admin'])
		) {
			$flux['data'] += billetteries_generer_commande($id_auteur);
		}
	}

	return $flux;
}

/**
 * Générer la commande suivant les billets en panier et met à jour `id_auteur_acheteur` pour ces derniers
 *
 * @param int id_auteur
 * 		Identifiant de l'utilisateur qui commande
 * @return array
 * 		Retourne un tableau des retours de création des objets Commande et Transaction
 **/
function billetteries_generer_commande($id_auteur) {
	include_spip('inc/session');
	include_spip('base/abstract_sql');
	include_spip('action/editer_objet');
	include_spip('action/editer_liens');
	include_spip('inc/commandes');
	include_spip('billetteries_fonctions');

	$retours = array();

	// Si on trouve des infos en panier qui ne sont PAS déjà dans une commande en cours du visiteur
	if (
		$id_auteur = intval($id_auteur)
		and $ids_billets = billetteries_panier_visiteur()
		and is_array($ids_billets)
		and !billetteries_id_commande_encours_visiteur($ids_billets)
	) {

		// Nettoyage : avant tout on dissocie toute commande qui serait déjà liée à ces billets
		// (mais pas de suppression car elle pourrait contenir autre chose)
		objet_dissocier(array('commande' => '*'), array('billet' => $ids_billets));

		// On crée une nouvelle commande et on l'associe aux billets,
		// ils seront validés lorsque celle-ci passera en payée
		if ($id_commande = creer_commande_encours($id_auteur)) {
			include_spip('inc/filtres');
			include_spip('inc/autoriser');

			// On remplit la commande avec le contenu du panier
			$ok = true;
			$sources_billets = array();
			foreach ($ids_billets as $id_billet) {
				// Si on trouve bien toujours le billet et donc son type
				if (
					$id_billet = intval($id_billet)
					and $billet = sql_fetsel('date, id_billets_type, source', 'spip_billets', 'id_billet = '.$id_billet)
					and $id_billets_type = intval($billet['id_billets_type'])
					and $billets_type = sql_fetsel('*', 'spip_billets_types', 'id_billets_type = '.$id_billets_type)
				) {
					// Quand on crée la commande, on est sûr d'avoir l'acheteur, donc on le remet pour tous les billets
					autoriser_exception('modifier', 'billet', $id_billet, true);
					objet_modifier('billet', $id_billet, array('id_auteur_acheteur'=>$id_auteur));
					autoriser_exception('modifier', 'billet', $id_billet, false);

					// La date de la commande sera celle d'un des billets panier
					$date_commande = $billet['date'];

					// Noter la source des billets
					$sources_billets[] = $billet['source'];

					// S'il n'y a pas déjà ce type dans la commande, on l'ajoute
					if (!$commandes_detail = sql_fetsel(
						'id_commandes_detail, quantite',
						'spip_commandes_details',
						array(
							'id_commande = '.$id_commande,
							'objet = "billets_type"',
							'id_objet = '.$id_billets_type
						)
					)) {
						// Récupérer le vrai prix ht du billet
						include_spip('prix_fonctions'); // paranoïa
						$prix_ht = prix_ht_objet($id_billet, 'billet');
						$id_commandes_detail = objet_inserer('commandes_detail', 0, array(
							'id_commande' => $id_commande,
							'descriptif' => $billets_type['titre'],
							'objet' => 'billets_type',
							'id_objet' => $id_billets_type,
							'quantite' => 1,
							'prix_unitaire_ht' => $prix_ht,
							'taxe' => $billets_type['taxe'],
						));

					}
					// Sinon on lui ajoute la quantité en plus
					else {
						$id_commandes_detail = intval($commandes_detail['id_commandes_detail']);

						objet_modifier(
							'commandes_detail',
							$id_commandes_detail,
							array(
								'quantite' => intval($commandes_detail['quantite']) + 1,
							)
						);
					}

					// Dans tous les cas, si on a bien un détail au final
					if ($id_commandes_detail) {
						// On lie le billet panier à la commande
						objet_associer(array('commande'=>$id_commande), array('billet'=>$id_billet));
					}
					// Sinon ya eu problème
					else {
						$ok = false;
					}

					//~ $ok = $ok && objet_inserer('commandes_detail', 0, array(
						//~ 'id_commande' => $id_commande,
						//~ 'descriptif' => $billets_type['titre'],
						//~ 'objet' => 'billet',
						//~ 'id_objet' => $id_billet,
						//~ 'quantite' => 1,
						//~ 'prix_unitaire_ht' => $billets_type['prix_ht'],
						//~ 'taxe' => $billets_type['taxe'],
					//~ ));
				}
			}

			// Si on a tout inséré
			if ($ok) {
				// On met à jour la commande avec la date et éventuellement la source
				$set_commande = array(
					'date' => $date_commande,
				);
				// Si tous les billets ont la même source, on la reprend dans la commande
				if (count(array_unique($sources_billets)) === 1) {
					$set_commande['source'] = $sources_billets[0];
				}
				objet_modifier('commande', $id_commande, $set_commande);

				// On retourne la référence et l'id de la commande créée
				$retours['id_commande'] = $id_commande;
				$retours['reference'] = sql_getfetsel(
					'reference',
					'spip_commandes',
					'id_commande=' . intval($id_commande)
				);
			}
			// Sinon on supprime tout et on met un message d'erreur
			else {
				objet_dissocier(array('commande'=>$id_commande), array('*'=>'*'));
				commandes_supprimer($id_commande);
				$retours['message_erreur'] = _T('billetteries:commander_erreur_transformation_panier');
				spip_log("[generer_commande] Commande non générée, auteur $id_auteur, billets " . implode(', ', $ids_billets), 'billetteries' . _LOG_ERREUR);
			}
		}
	}

	return $retours;
}

/**
 * Agir avant l'insertion en base lors de la création des objets
 *
 * - Forcer le id_billets_type sur les billets
 *
 * @note
 * La création d'un billet se fait parfois en 2 étapes, notamment avec les forms d'édition :
 * l'objet est d'abord créé à vide avec objet_inserer,
 * puis les valeurs sont ajoutées avec objet_modifier.
 * On force à enregistrer le id_billets_type dès la 1ère étape,
 * sinon ça peut poser des problèmes, pour récupérer le prix dans pre_edition notamment.
 *
 * @pipeline pre_insertion
 * @param array $flux
 *   Flux du pipeline contenant le contexte et les couples champ/valeurs à insérer
 * @return array
 *   Retourne le flux possiblement modifié
 **/
function billetteries_pre_insertion($flux) {

	$table = (isset($flux['args']['table']) ? $flux['args']['table'] : null);

	// Si on crée un billet et qu'on a un type de billet
	if (
		$table === 'spip_billets'
		and $id_billets_type = _request('id_billets_type')
	) {
		$flux['data']['id_billets_type'] = $id_billets_type;
	}

	return $flux;
}

/**
 * Agir après l'insertion en base lors de la création des objets
 *
 * - Auto-compléter certains champs des billets
 *   Désactivable ponctuellement avec `billetteries_desactiver_auto_completion` à true dans le request
 *   Nb : ne peut se faire dans pre_insertion car on a besoin du n° du billet
 *
 * @uses billetteries_billet_completer_champs()
 *
 * @note
 * La création d'un objet se fait parfois en 2 étapes, notamment avec les forms d'édition :
 * l'objet est d'abord créé à vide avec objet_inserer,
 * puis les valeurs sont ajoutées avec objet_modifier.
 * Dans ce cas il ne faut rien faire ici car les modifs seront écrasées,
 * on agira dans pre_edition plutôt.
 *
 * @uses billetteries_billet_completer_champs()
 *
 * @pipeline post_insertion
 * @param array $flux
 *   Flux du pipeline contenant le contexte et les couples champ/valeurs à insérer
 * @return array
 *   Retourne le flux possiblement modifié
 **/
function billetteries_post_insertion($flux) {

	$table = (isset($flux['args']['table']) ? $flux['args']['table'] : null);
	$id_objet = (isset($flux['args']['id_objet']) ? intval($flux['args']['id_objet']) : 0);

	// Si on crée un billet, auto-compléter certains champs.
	// Ici peu importe le statut contrairement à post_edition.
	// Désactivable ponctuellement avec `billetteries_desactiver_auto_completion` à true dans le request.
	if (
		_request('billetteries_desactiver_auto_completion') !== true
		and $table === 'spip_billets'
		and $id_billet = $id_objet
		// Si le $set est vide ou qu'il ne contient que le statut et la date,
		// ça veut dire que la création se fait en 2 temps (ex. : formulaire d'édition).
		// Ne rien faire ici car les modifs seront écrasées, on agira dans pre_edition.
		and (
			count($flux['data']) > 0
			and !(
				count($flux['data']) === 2
				and array_keys($flux['data']) === array('statut', 'date')
			)
		)
		and $billet = sql_fetsel('*', 'spip_billets', 'id_billet = ' . $id_billet)
	) {
		include_spip('inc/billetteries');
		$champs = $flux['data'];
		$champs_modifs = billetteries_billet_completer_champs($id_billet, $champs, $billet);
		// On met à jour s'il y a des modifs
		if ($set = array_diff($champs_modifs, $champs)) {
			include_spip('action/editer_objet');
			$log_set = json_encode($set);
			spip_log("[post_insertion] compléments automatiques du nouveau billet $id_billet : $log_set", 'billetteries' . _LOG_DEBUG);
			objet_modifier('billet', $id_billet, $set);
		}
		// Et retourner les champs modifiés
		$flux['data'] = $champs_modifs;
	}

	return $flux;
}

/**
 * Faire des choses avant l'édition d'un objet
 *
 * - Auto-compléter certains champs des billets tant qu'ils sont panier
 *   Désactivable ponctuellement avec `billetteries_desactiver_auto_completion` à true dans le request
 *
 * @uses billetteries_billet_completer_champs()
 *
 * @note
 * On passe parfois dans ce pipeline même en cas de création d'objet.
 * Voir la @note de post_insertion.
 *
 * @uses billetteries_billet_completer_champs()
 *
 * @pipeline pre_edition
 * @param array $flux
 *   Flux du pipeline contenant le contexte et les couples champ/valeurs à modifier
 * @return array
 *   Retourne le flux possiblement modifié
 */
function billetteries_pre_edition($flux) {

	$table = (isset($flux['args']['table']) ? $flux['args']['table'] : null);
	$id_objet = (isset($flux['args']['id_objet']) ? intval($flux['args']['id_objet']) : 0);
	$action = (isset($flux['args']['action']) ? $flux['args']['action'] : null);

	// Si on modifie un billet au panier, auto-compléter certains champs
	// Désactivable ponctuellement avec `billetteries_desactiver_auto_completion` à true dans le request
	if (
		_request('billetteries_desactiver_auto_completion') !== true
		and $table === 'spip_billets'
		and $id_billet = $id_objet
		and $action === 'modifier' // attention on a ça même quand ça institue (?)
		and (
			// soit on est en train d'instituer un nouveau statut
			$statut = $flux['args']['data']['statut'] ?? null
			// soit on récupère le statut actuel
			or $statut = sql_getfetsel('statut', 'spip_billets', "id_billet = $id_billet")
		)
		and $statut === 'panier'
	) {
		include_spip('inc/billetteries');
		$champs = $flux['data'];
		$billet = sql_fetsel('*', 'spip_billets', "id_billet = $id_billet");
		$champs_modifs = billetteries_billet_completer_champs($id_billet, $champs, $billet);
		// On log s'il y a des modifs
		if ($set = array_diff($champs_modifs, $champs)) {
			$log_set = json_encode($set);
			spip_log("[pre_edition] compléments automatiques du billet $id_billet au panier : $log_set", 'billetteries' . _LOG_DEBUG);
		}
		// Et retourner les champs modifiés
		$flux['data'] = $champs_modifs;
	}

	return $flux;
}

/**
 * Faire des choses après l'édition d'un objet
 *
 * - Valider les billets quand une commande est payée
 * - Appeler les notifs quand un billet est annulé (WIP)
 *
 * @pipeline post_edition
 * @param array $flux
 *   Flux du pipeline contenant le contexte et les couples champ/valeurs modifiées
 * @return array
 *   Retourne le flux possiblement modifié
 */
function billetteries_post_edition($flux) {

	$table = (isset($flux['args']['table']) ? $flux['args']['table'] : null);
	$id_objet = (isset($flux['args']['id_objet']) ? intval($flux['args']['id_objet']) : 0);
	$action = (isset($flux['args']['action']) ? $flux['args']['action'] : null);

	// Commande : si on la paye il faut valider les billets dedans
	if (
		// Si on institue une commande
		$table === 'spip_commandes'
		and $id_commande = $id_objet
		and $action === 'instituer'
		// Et qu'on passe en statut "paye" depuis autre chose
		and isset($flux['data']['statut'])
		and $flux['data']['statut'] === 'paye'
		and $flux['args']['statut_ancien'] !== 'paye'
		// Et que la commande existe bien
		and sql_countsel('spip_commandes', 'id_commande = ' . $id_commande)
		// Et qu'on a des billets liés à cette commande
		and include_spip('action/editer_liens')
		and $liens = objet_trouver_liens(array('commande'=>$id_commande), array('billet'=>'*'))
		and is_array($liens)
	) {
		include_spip('action/editer_objet');

		foreach ($liens as $lien) {
			// Si on trouve un billet
			if ($id_billet = intval($lien['id_objet'])) {
				// On passe le billet en validé
				// TODO : mettre à jour certaines infos au cas-où (prix_ht, taxe, titre)
				autoriser_exception('instituer', 'billet', $id_billet, true);
				$set = array('statut' => 'valide');
				objet_modifier('billet', $id_billet, $set);
				autoriser_exception('instituer', 'billet', $id_billet, false);
			}
		}
	}
	// Si c'est un billet annulé, il faut envoyer certaines notifications
	// WIP : ces notifs n'existent pas encore + rendre configurable
	elseif (
		$table === 'spip_billets'
		and $id_billet = $id_objet
		and sql_countsel('spip_billets', 'id_billet = ' . $id_billet)
		and $action === 'instituer'
		and $flux['data']['statut'] === 'annule'
		and $flux['data']['statut_ancien'] !== 'annule'
	) {
		// Notifications
		if ($notifications = charger_fonction('notifications', 'inc')) {
			// Pour les admins
			$notifications('billet_annulation_admin', $id_billet, array('billet'=>$billet));
			// Pour les acheteurs
			$notifications('billet_annulation_acheteur', $id_billet, array('billet'=>$billet));
			// Pour les participants
			$notifications('billet_annulation_participant', $id_billet, array('billet'=>$billet));
		}
	}

	return $flux;
}

/**
 * Protéger le formulaire de mise au panier de billets
 *
 * @param array $flux
 *     Liste d'identifiants de formulaires
 * @return array
 *     Liste complétée
 */
function billetteries_nospam_lister_formulaires($flux) {
	if (!in_array('commander_billetterie', $flux)) {
		$flux[] = 'commander_billetterie';
	}

	return $flux;
}
