<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Billetteries
 * @copyright  2019
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Billetteries\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function billetteries_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['billetteries'] = 'billetteries';
	$interfaces['table_des_tables']['billets_types'] = 'billets_types';
	$interfaces['table_des_tables']['billets'] = 'billets';

	$interfaces['table_des_traitements']['INFOS']['billets'] = 'unserialize(%s)';
	$interfaces['table_des_traitements']['DATE_DEBUT']['billetteries'] = 'filtrer_date_vide(%s)';
	$interfaces['table_des_traitements']['DATE_FIN']['billetteries'] = 'filtrer_date_vide(%s)';
	$interfaces['table_des_traitements']['DATE_DEBUT']['billets_types'] = 'filtrer_date_vide(%s)';
	$interfaces['table_des_traitements']['DATE_FIN']['billets_types'] = 'filtrer_date_vide(%s)';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function billetteries_declarer_tables_objets_sql($tables) {
	$tables['spip_billetteries'] = array(
		'type' => 'billetterie',
		'principale' => 'oui',
		'field'=> array(
			'id_billetterie'     => 'bigint(21) NOT NULL',
			'titre'              => 'text NOT NULL DEFAULT ""',
			'descriptif'         => 'text NOT NULL DEFAULT ""',
			'quota'              => 'int(11) NOT NULL DEFAULT 0',
			'liste_attente'      => 'int(1) NOT NULL DEFAULT 0',
			'date_fin'           => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'date_debut'         => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'statut'             => 'varchar(20)  DEFAULT "0" NOT NULL',
			'maj'                => 'TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_billetterie',
			'KEY statut'         => 'statut',
		),
		'titre' => 'titre AS titre, "" AS lang',
		'date' => '',
		'champs_editables'  => array('titre', 'descriptif', 'quota', 'liste_attente', 'date_debut', 'date_fin'),
		'champs_versionnes' => array('titre', 'descriptif', 'quota', 'liste_attente', 'date_debut', 'date_fin'),
		'rechercher_champs' => array('titre' => 5),
		'tables_jointures'  => array('spip_billetteries_liens'),
		'statut_textes_instituer' => array(
			'inactif' => 'billetterie:texte_statut_inactif',
			'actif'   => 'billetterie:texte_statut_actif',
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'actif',
				'previsu'   => 'inactif',
				'post_date' => 'date_debut',
				'exception' => array('statut','tout')
			)
		),
		'statut_images' => array(
			'actif'   => 'puce-publier-8.png',
			'inactif' => 'puce-proposer-8.png',
		),
		'texte_changer_statut' => 'billetterie:texte_changer_statut_billetterie',
	);

	$tables['spip_billets_types'] = array(
		'type' => 'billets_type',
		'principale' => 'oui',
		'table_objet_surnoms' => array('type_billet', 'typebillet', 'billetstype'), // table_objet('billets_type') => 'billets_types'
		'field'=> array(
		'id_billets_type'    => 'bigint(21) NOT NULL',
		'id_billetterie'     => 'bigint(21) NOT NULL DEFAULT 0',
		'rang'               => 'int NOT NULL DEFAULT 0',
		'titre'              => 'text NOT NULL DEFAULT ""',
		'descriptif'         => 'text NOT NULL DEFAULT ""',
		'quota'              => 'int(11) NOT NULL DEFAULT 0',
		'prix_ht'            => 'decimal(20,6) NOT NULL DEFAULT 0',
		'taxe'               => 'decimal(4,4) NOT NULL DEFAULT 0',
		'selection_min'      => 'int(11) NOT NULL DEFAULT 0',
		'selection_max'      => 'int(11) NOT NULL DEFAULT 0',
		'date_fin'           => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
		'date_debut'         => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
		'maj'                => 'TIMESTAMP'
	),
		'key' => array(
			'PRIMARY KEY'        => 'id_billets_type',
			'KEY id_billetterie'  => 'id_billetterie',
		),
		'join' => array(
			'id_billetterie' => 'id_billetterie',
		),
		'titre' => 'titre AS titre, "" AS lang',
		'date' => '',
		'champs_editables'  => array('rang', 'titre', 'descriptif', 'quota', 'prix_ht', 'taxe', 'date_debut', 'date_fin', 'id_billetterie', 'id_profil', 'selection_min', 'selection_max'),
		'champs_versionnes' => array('titre', 'descriptif', 'quota', 'prix_ht', 'taxe', 'date_debut', 'date_fin', 'id_billetterie', 'id_profil', 'selection_min', 'selection_max'),
		'rechercher_champs' => array('titre' => 5),
		'tables_jointures'  => array(),
	);

	$tables['spip_billets'] = array(
		'type' => 'billet',
		'principale' => 'oui',
		'page' => false,
		'field'=> array(
			'id_billet'          => 'bigint(21) NOT NULL',
			'id_billets_type'    => 'bigint(21) NOT NULL DEFAULT 0',
			'id_billetterie'     => 'bigint(21) NOT NULL DEFAULT 0',
			'id_auteur'          => 'int(11) NOT NULL DEFAULT 0',
			'id_auteur_acheteur' => 'int(11) NOT NULL DEFAULT 0',
			'source'             => 'varchar(255) NOT NULL DEFAULT ""', // ex. : commander_billetterie#xx
			'code'               => 'varchar(255) NOT NULL DEFAULT ""',
			'email'              => 'varchar(255) NOT NULL DEFAULT ""',
			'date'               => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'date_annulation'    => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'infos'              => 'text NOT NULL DEFAULT ""',
			'cookie'             => 'varchar(255) NOT NULL DEFAULT ""',
			'statut'             => 'varchar(20) DEFAULT "panier" NOT NULL',
			'maj'                => 'TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'            => 'id_billet',
			'KEY code'               => 'code',
			'KEY id_billets_type'    => 'id_billets_type',
			'KEY id_billetterie'     => 'id_billetterie',
			'KEY id_auteur'          => 'id_auteur',
			'KEY id_auteur_acheteur' => 'id_auteur_acheteur',
			'KEY statut'             => 'statut',
		),
		'join' => array(
			'id_billets_type'    => 'id_billets_type',
			'id_billetterie'     => 'id_billetterie',
			'id_auteur'          => 'id_auteur',
			'id_auteur_acheteur' => 'id_auteur',
		),
		'titre' => 'code AS titre, "" AS lang',
		// 'titre' => 'concat((select titre from spip_billets_types where id_billets_type=spip_billets.id_billets_type limit 0,1), (case when code then concat(" / ", code) end)) AS titre, "" AS lang',
		'date' => 'date',
		'champs_editables'  => array('date_annulation', 'id_auteur', 'id_auteur_acheteur', 'source', 'code', 'email', 'id_billets_type', 'infos', 'cookie'),
		'champs_versionnes' => array('date_annulation', 'id_auteur', 'id_auteur_acheteur', 'source', 'code', 'email', 'id_billets_type', 'infos', 'cookie'),
		'rechercher_champs' => array("code" => 8),
		'tables_jointures'  => array(),
		'statut_textes_instituer' => array(
			'panier'     => 'billet:texte_statut_panier', // créé, mais pas encore payé
			'attente'    => 'billet:texte_statut_attente', // en liste d'attente (plus de billet dispo)
			'valide'     => 'billet:texte_statut_valide', // payé
			'utilise'    => 'billet:texte_statut_utilise', // payé et utilisé
			'annule'     => 'billet:texte_statut_annule', // annulé manuellement par admin
			'abandonne'  => 'billet:texte_statut_abandonne', // annulé automatiquement après délai dépassé
			'poubelle'   => 'texte_statut_poubelle', // poubelle
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'valide,utilise',
				'previsu'   => 'temporaire',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'statut_images' => array(
			'panier'     => 'puce-preparer-8.png', // créé, mais pas encore payé
			'attente'    => 'puce-proposer-8.png', // en liste d'attente (plus de billet dispo)
			'valide'     => 'puce-billet-valide.png', // payé
			'utilise'    => 'puce-publier-8.png', // payé et utilisé
			'annule'     => 'puce-refuser-8.png', // annulé manuellement par admin
			'abandonne'  => 'puce-billet-abandonne.png', // annulé automatiquement après délai dépassé
			'poubelle'   => 'puce-supprimer-8.png', // poubelle
		),
		'texte_changer_statut' => 'billet:texte_changer_statut_billet',
	);

	return $tables;
}


/**
 * Déclaration des tables secondaires (liaisons)
 *
 * @pipeline declarer_tables_auxiliaires
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function billetteries_declarer_tables_auxiliaires($tables) {
	$tables['spip_billetteries_liens'] = array(
		'field' => array(
			'id_billetterie'      => 'bigint(21) DEFAULT "0" NOT NULL',
			'id_objet'           => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet'              => 'VARCHAR(25) DEFAULT "" NOT NULL',
			'vu'                 => 'VARCHAR(6) DEFAULT "non" NOT NULL',
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_billetterie,id_objet,objet',
			'KEY id_billetterie'  => 'id_billetterie',
		)
	);

	return $tables;
}
